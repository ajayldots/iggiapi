<?php
require(APPPATH.'/libraries/REST_Controller.php');
class Api extends REST_Controller{
    public function __construct()    {
        parent::__construct();
        $this->load->model('Iggi_model');
        $this->load->database();
    }

    //this api is used  to invite mentor  from prime mentor
    function setInviteMentorByprimeMentor(){
      $mentor_id=$this->input->post('mentor_id');

    }
    //this api is used to show list of mentee and social media status as per mentor_id
    function getmenteeListwithSocialStatus_post(){
     $mentor_id=$this->input->post('mentor_id');
     $user_mentor_arr=getUserById($mentor_id);
     $user_mentor_name=$user_mentor_arr[0]->first_name." ".$user_mentor_arr[0]->last_name;
     $mentor_info_arr['mentor_info'] = array(
       'mentor_name' => $user_mentor_name,
       'mentor_id' => $mentor_id,
       'profile_url'=>'http://dev.iggiapp.com/uploads/profile/fDgXoe1F040421180418download.jpeg'
     );

     $data_arr_show=array();
      if($mentor_id==""){
        $response['data']='';
        $response['message']= "mentor_id is required";
        $response['status']= 0;
        $this->response($response, 200);
      }else{
           $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>'1','added_by'=>$mentor_id));
           if($query->num_rows() > 0){
             $data_arr=$query->result();

             foreach ($data_arr as $key => $value) {
                $mentee_id=$value->uid;
                $user_arr=getUserById($mentee_id);
                $user_name=$user_arr[0]->first_name." ".$user_arr[0]->last_name;
                $user_info_arr = array(
                  'user_name' => $user_name,
                  'profile_url'=>'http://dev.iggiapp.com/uploads/profile/fDgXoe1F040421180418download.jpeg'

                );

                //-------------------
                $gooleplus_arr=getGooglePLUSUser($mentee_id);
                $gooleplus_arr = array_filter($gooleplus_arr);
                $gplus=0;
                if (!empty($gooleplus_arr)) {
                  $gplus=1;
                }
                $instagram_arr=getInstagramUser($mentee_id);
                $instagram_arr = array_filter($instagram_arr);
                $inst=0;
                if (!empty($instagram_arr)) {
                  $inst=1;
                }


                $linkedin_arr=getLinkedinUser($mentee_id);
                $linkedin_arr = array_filter($linkedin_arr);
                $li=0;
                if (!empty($linkedin_arr)) {
                    $li=1;
                }

                $facebook_arr=getFacebookUser($mentee_id);
                $facebook_arr = array_filter($facebook_arr);
                $fb=0;
                if (!empty($facebook_arr)) {
                  $fb=1;
                }
                  $twitter_arr=getTwitterUser($mentee_id);
                  $twitter_arr = array_filter($twitter_arr);
                  $tw=0;
                  if (!empty($twitter_arr)) {
                    $tw=1;
                  }
                  $arrayFbSocial = array(
                    'facebook' =>$fb ,
                    'twitter' =>$tw ,
                    'linkedin'=>$li,
                    'instagram'=>$inst,
                    'google_plus'=>$gplus,

                  );
                //-------------------

                $data_arr_show['mentee_info'][]=array_merge($arrayFbSocial,(array)$value,$user_info_arr);



             }

             $response['data']=array_merge($mentor_info_arr,$data_arr_show);
             $response['message']= "list of mentee with social status";
             $response['status']= 1;
             $this->response($response, 200);


           }else{
             $mentee_info['mentee_info']=array(

             );
             $response['data']=array_merge($mentor_info_arr,$mentee_info);
             $response['message']= "no mentee is added yet";
             $response['status']= 0;
             $this->response($response, 200);
           }
      }
    }
    //this api is used to show which social media link  is linked
    function getSocilMediaLinkedStatus_post(){
       $mentee_id=$this->input->post('mentee_id');

      if($mentee_id==""){
        $response['data']='';
        $response['message']= "mentee_id is required";
        $response['status']= 0;
        $this->response($response, 200);
      }else{
        $gooleplus_arr=getGooglePLUSUser($mentee_id);
        $gooleplus_arr = array_filter($gooleplus_arr);
        $gplus=0;
        if (!empty($gooleplus_arr)) {
          $gplus=1;
        }
        $instagram_arr=getInstagramUser($mentee_id);
        $instagram_arr = array_filter($instagram_arr);
        $inst=0;
        if (!empty($instagram_arr)) {
          $inst=1;
        }


        $linkedin_arr=getLinkedinUser($mentee_id);
        $linkedin_arr = array_filter($linkedin_arr);
        $li=0;
        if (!empty($linkedin_arr)) {
            $li=1;
        }

        $facebook_arr=getFacebookUser($mentee_id);
        $facebook_arr = array_filter($facebook_arr);
        $fb=0;
        if (!empty($facebook_arr)) {
          $fb=1;
        }
          $twitter_arr=getTwitterUser($mentee_id);
          $twitter_arr = array_filter($twitter_arr);
          $tw=0;
          if (!empty($twitter_arr)) {
            $tw=1;
          }
          $arrayFbSocial = array(
            'facebook' =>$fb ,
            'twitter' =>$tw ,
            'linkedin'=>$li,
            'instagram'=>$inst,
            'google_plus'=>$gplus,

          );
          $response['data']=$arrayFbSocial;
          $response['message']= "social liked list";
          $response['status']= 1;
          $this->response($response, 200);
      }




    }
   //this api is used to send request to metor by mentee

   function sendRequestMentor_post(){
     $txtMentorInviteEmail=$this->input->post('txtMentorInvite');
     if($txtMentorInviteEmail==""){
       $response['data']='';
       $response['message']= "txtMentorInvite is required";
       $response['status']= 1;
       $this->response($response, 200);
     }else{
       $this->load->library('SendEmail', null, 'mailObj');
       $strpEmailTo = $txtMentorInviteEmail;
       $strpRecipientName = '';
       $arrpTags = array();
       $arrpTags['{--SiteHomeURL--}'] = base_url();
       $arrpTags['{--SiteHomeURLlogo--}'] = base_url()."theme/images/logo.png";
       $arrpTags['{--Subject--}'] = "IGGI Invitation";
       $arrpTags['{--LoginUrl--}'] = base_url();
       $strpTempleteid = 6;
       if($this->mailObj->sendEmail(
          $strpEmailTo,
          $strpRecipientName,
          $arrpTags,
          $strpTempleteid,
          $AdminEmailId = '',
          $AdminName = '')) ;
          $response['data']='';
          $response['message']= "sent request successfully";
          $response['status']= 1;
          $this->response($response, 200);
     }




   }
   //this api is used to show all pending post by of mentee
   function getAllPost_post(){
       $mentee_id=$this->input->post('mentee_id');
       if($mentee_id==""){
         $response['data']='';
         $response['message']= "mentee_id is required";
         $response['status']= 0;
       }else {
         $query = $this->db->get_where('tbl_mymentor_posts',array('uid'=>$mentee_id));
            if($query->num_rows() > 0){
            $arr_data=$query->result();
            $response['data']=$arr_data;
            $response['message']= "list of all pending posts";
            $response['status']= 1;
          }else{
            $response['data']='';
            $response['message']= "no record found!";
            $response['status']= 0;
          }

         $this->response($response, 200);
       }

   }

    //this api is used to show all pending post by of mentee
    function getPendingPost_post(){
        $mentee_id=$this->input->post('mentee_id');
        if($mentee_id==""){
          $response['data']='';
          $response['message']= "mentee_id is required";
          $response['status']= 0;
        }else {
          $query = $this->db->get_where('tbl_mymentor_posts',array('status'=>'0','uid'=>$mentee_id));
             if($query->num_rows() > 0){
             $arr_data=$query->result();
             $response['data']=$arr_data;
             $response['message']= "list of all pending posts";
             $response['status']= 1;
           }else{
             $response['data']='';
             $response['message']= "no record found!";
             $response['status']= 0;
           }

          $this->response($response, 200);
        }

    }
    //this api is used to show all post by social media request
    function getActionAppliedbySocial_post(){
         $mentee_id=$this->input->post('mentee_id');
         $social_media_name=$this->input->post('social_media_name');
         if($mentee_id=="" && $social_media_name=="" ){
           $response['data']=$arr_data;
           $response['message']= "mentee_id and social_media_name";
           $response['status']= 0;

         }else{
           $where = "uid=$mentee_id AND status='2' OR status='1' and";
           $where.= " social_media_type='".$social_media_name."'";
           $this->db->where($where);
           $query = $this->db->get('tbl_mymentor_posts');
           $str = $this->db->last_query();
           $arr_data=$query->result();
           // $query = $this->db->get_where('tbl_mymentor_posts',array('status'=>'2','uid'=>$mentee_id));
            if($query->num_rows() > 0){
              $arr_data=$query->result();
              $response['data']=$arr_data;
              $response['message']= "list of all pending posts";
              $response['status']= 1;
            }else{
              $response['data']='';
              $response['message']= "no record found!";
              $response['status']= 0;
            }

           $this->response($response, 200);
         }


    }

    function getActionAppliedPost_post(){
        $mentee_id=$this->input->post('mentee_id');
        if($mentee_id==""){
          $response['data']='';
          $response['message']= "no record found!";
          $response['status']= 0;
          $this->response($response, 200);
        }else{
          $where = "uid=$mentee_id AND status='2' OR status='1'";
          $this->db->where($where);
          $query = $this->db->get('tbl_mymentor_posts');
          $str = $this->db->last_query();
          $arr_data=$query->result();
          // $query = $this->db->get_where('tbl_mymentor_posts',array('status'=>'2','uid'=>$mentee_id));
           if($query->num_rows() > 0){
             $arr_data=$query->result();
             $response['data']=$arr_data;
             $response['message']= "list of all pending posts";
             $response['status']= 1;
           }else{
             $response['data']='';
             $response['message']= "no record found!";
             $response['status']= 0;
           }
          $this->response($response, 200);
        }

    }
    //this api is used to send post  for approval
    function sendSocialPost_post(){
       $user_id=$this->input->post('user_id');
       $social_type=$this->input->post('social_type');
       $message=$this->input->post('message');
       $type_name=$this->input->post('type');
       $link_url=$this->input->post('link_url');
       $link_picture=$this->input->post('link_picture');
       $link_title=$this->input->post('link_title');
       $link_caption=$this->input->post('link_caption');
       $link_description=$this->input->post('link_description');
       $image_url=$this->input->post('image_url');
       $video_url=$this->input->post('video_url');
       $txtPostTitle=$this->input->post('txtPostTitle');
       if($type_name=='images'){
         $image_url=json_encode($this->input->post('images_url[]'));
       }else {
         $image_url=$this->input->post('image_url');
       }
       if($type_name=='video'){
         $link_url=$video_url;
       }else {
         $link_url=$this->input->post('link_url');

       }
       $myMentor_arr=get_all_active_Mentor_byMentee($user_id);
   		$userNew_arr=getUserById($user_id);
   		$userNew_Name=$userNew_arr[0]->first_name." ".$userNew_arr[0]->last_name;


       //start of twitter
       if($social_type=='twitter_post'){

        $user_id=$user_id;
        $this->db->select("*");
        $this->db->from('tbl_twitter');
        $this->db->where('uid', $user_id);
        $query = $this->db->get();
        $result_array = $query->result_array();

        foreach ($result_array as $key => $value) {
         $name=$value['name'];
         $name=$value['screen_name'];
         $tid=$value['tid'];
        }
           $data = array(
           'type' => $type_name ,
           'fid' => $tid ,
           'account' => $user_id,
           'name'=>$name,
           'message'=>$message,
           'time_post'=>date('Y-m-d h:i:s'),
           'url'=>$link_url,
           'uid'=>$user_id,
           'image' =>$image_url,
           'title'=>$link_title,
           'post_title'=>$txtPostTitle,
           'description'=>$message,
           'status'=>0											);

            $this->db->insert('tbl_posts', $data);
            $post_id=$this->db->insert_id();
            $spintax = new Spintax();
            ini_set('max_execution_time', 300000);
            define("TIMEPOST",date("Y-m-d H:i").":00");
            $result = $this->db
              ->select('*')
              ->from('tbl_posts')
              ->where('status = ', 0)
               ->where('account= ', $user_id)
              ->get()->result();
              if(!empty($result)){
              foreach ($result as $key => $row) {
                $delete       = $row->delete;
                $repeat       = $row->repeat_post;
                $repeat_time  = $row->repeat_time;
                $repeat_end   = $row->repeat_end;
                $time_post    = $row->time_post;
                $deplay       = $row->deplay;

                $time_post          = strtotime($time_post);
                $time_post_only_day = date("Y-m-d", $time_post);
                $time_post_day      = strtotime($time_post_only_day);
                $repeat_end         = strtotime($repeat_end);

                $row->type         = $spintax->process($type_name);
                $row->url         = $spintax->process($link_url);
                $row->message     = $spintax->process($message);
                $row->title       = $spintax->process($link_title);
                $row->description = $spintax->process($link_description);
                $row->image       = $spintax->process($image_url);
                $row->caption     = $spintax->process($link_caption);
                $TW = $this->db->get_where('tbl_twitter',array('uid'=>$row->account));
               if(!empty($TW)){
               $row->access_token = $TW->result()[0]->access_token;
              //---------------Save data on Successfully post--if auto post not active ----------------------------
              $auto_post=0;
              if($auto_post==1){
                //post now
                  $response = TW_POST($row);
                  $msg="Your post successfully posted.";
              }else{
                //save post to approved  then post
                //tbl_mymentor_posts
                $data = array(
                'post_title'=>$txtPostTitle,
                 'social_media_type'=>'twitter',
                 'type' => $type_name,
                 'fid' =>$tid,
                 'account' => $user_id,
                  'name'=>$name,
                 'access_token' =>$row->access_token,
                 'cid' =>'',
                 'message' =>$message,
                 'title'=>$link_title,
                 'description'=>$link_description,
                 'url'=>$link_url,
                 'image' =>$image_url,
                 'caption' =>$link_caption,
                 'time_post' =>date('Y-m-d h:i:s'),
                 'uid' => $user_id,
                 'status' => 0,
                 'created'=>date('Y-m-d h:i:s')
              );

              $id=$this->db->insert('tbl_mymentor_posts', $data);


             $post_link=BASE."index.php/posts/view/".$id;
             $post_userid=$post_info_arr[0]->uid;
             $user_post_arr=getUserById($myMentor_arr[0]);
             $mentee_emailid=$user_post_arr[0]->email;
             date_default_timezone_set('UTC');
             $this->load->library('SendEmail', null, 'mailObj');
             $strpEmailTo =$mentee_emailid;
             $strpRecipientName = '';
             $arrpTags = array();
             $arrpTags['{--ApprovalMenteeName--}'] = $userNew_Name;
             $arrpTags['{--PostTitle--}'] = $txtPostTitle;
             $arrpTags['{--PostLink--}'] = $post_link;
             $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
             $arrpTags['{--SocialPlatform--}'] = 'Twitter';
             $arrpTags['{--SiteHomeURL--}'] = base_url();
             $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

             $arrpTags['{--Subject--}'] = "IGGI | Post Sending for Approval";
             $arrpTags['{--LoginUrl--}'] = base_url();
             $strpTempleteid = 14;
             if($this->mailObj->sendEmail(
                $strpEmailTo,
                $strpRecipientName,
                $arrpTags,
                $strpTempleteid,
                $AdminEmailId = '',
                $AdminName = '')) ;

                $response['data']='';
                $response['message']= "Posted new post for twitter";
                $response['status']= 1;


            }
            //end if else


            }
          }
        }
       }

      $this->response($response, 200);

    }
    //this api for social login for mentor facebook
    function setSocialMentor_post(){
       $login_type=$this->input->post('login_type');
       $facebook_id=$this->input->post('login_id');
       $facebook_email=$this->input->post('login_email');
       $facebook__fullname=$this->input->post('login__fullname');
       $facebook__profile_pic=$this->input->post('login__profile_pic');
       $data = array(
        "type"            => $login_type,
        "pid"             => $facebook_id,
        "fullname"        => $facebook__fullname,
        "status"          => '1',
        "timezone"				=>'Asia/Kolkata',
        'first_name'			=>'',
        'last_name'			=>'',
        'profile_pic'		=>$facebook__profile_pic,
      );
        if(isset($facebook_email)){
          $data['email'] = $facebook_email;
        }
      //  print_r($data);
        if(isset($data['email'])){
          $result = $this->db->get_where('user_management',array('email'=>$facebook_email));
			}else{
         $result = $this->db->get_where('user_management',array('pid'=>$facebook_id));
			}
       $result_arr=$result->result();
       $result_arr = array_filter($result_arr);
       if (!empty($result_arr)){
          $this->db->update('user_management', $data, array('id' => $result_arr[0]->id));
          $response['data']='';
          $response['message']= "Updated information";
          $response['status']= 1;
        }
      else
        {
           $this->db->insert('user_management', $data);
           $id = $this->db->insert_id();
           $result = $this->db->get_where('user_management',array('id'=>$id));
            $plan=75;//this is basic plan id need to update with function
            $subs_arr=get_subscriptions_byid($plan);
            $subs_interval=$subs_arr[0]->subs_interval;
            switch ($subs_interval) {
              case 'lifetime':
                $exp_days="+10 years";
                $date = strtotime(date('Y-m-d H:i:s'));
                $date_exp = strtotime($exp_days, $date);
                $futue_date= date('Y-m-d H:i:s', $date_exp);
                break;
              default:
                # code...
                break;
            }
            $defaut_plan_arr = array(
            'plan_type' =>'general',
            'plan_name' =>$subs_arr[0]->plan_name,
            'user_id' =>$id,
            'plan_id' =>$subs_arr[0]->id,
            'plan_details' =>'',
            'no_mentee'=>$subs_arr[0]->no_mentee,
            'no_mentor'=>$subs_arr[0]->no_mentor,
            'no_platform'=>$subs_arr[0]->no_platform,
            'no_device'=>$subs_arr[0]->no_platform,
            'created_on'=>date('Y-m-d H:i:s'),
            'updated_on'=>date('Y-m-d H:i:s'),
            'expired_on'=>$futue_date,
            'status'=>'3',
            'qty'=>'1',
            'plan_interval'=>$subs_interval
          );
          $this->db->insert('tbl_membership', $defaut_plan_arr);
          $response['data']='';
          $response['message']= "Inserted new mentor";
          $response['status']= 1;

        }
         $this->response($response, 200);



    }
    //this function is used to upload profile picture
    function setProfilePic_post(){
      var_dump( $this->input->post('profile_pic') );
    }
    //this function is used to get the list of mentor type
    function getMentorType_get(){

        $query = $this->db->get('tbl_mentor_type');
         if($query->num_rows() > 0){
           $arr=$query->result();
           $array_data = array();
           foreach ($arr as $key => $value) {
             $array_data[]=
             array(
               'id'=>intval($value->mentor_name),
                'mentor_name'=>is_null($value->mentor_name) ? '':$value->mentor_name
             );
              }
            $response['data']=$array_data;
            $response['message']= " List of mentor type";
            $response['status']= 1;

        }else{
          $response['data']='';
          $response['message']= " No mentor type found";
          $response['status']= 0;
        }

           $this->response($response, 200);

    }

    //this function is used to get the list of pending
    function getPendingListofMenteeInvitation_post(){
        $mentorid=$this->input->post('mentorid');
        $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"0",'added_by'=>$mentorid));
         if($query->num_rows() > 0){
         $arr=$query->result();

            $response['data']=$arr[0];
            $response['message']= " List of pending Mentee";
            $response['status']= 1;

        }else{
          $response['data']='';
          $response['message']= " No pending mentee list";
          $response['status']= 0;
        }

           $this->response($response, 200);

    }
    //this is used to create list of all mentee by mentor_email
    function menteeListByMentorId_post(){
       $mentorid=$this->input->post('mentorid');
       $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"1",'added_by'=>$mentorid,'user_type'=>"1"));
       $arr=$query->result();
       if($query->num_rows() > 0){
          $response['data']= $arr;
          $response['message']= " List of mentee";
          $response['status']= 1;
        }else{
          $response['data']= '';
          $response['message']="No mentee found";
          $response['status']= 0;
        }
        $this->response($response, 200);

    }
    //this is used to create list of all mentee by mentor_email
    function mentorListByPrimeMentorId_post(){
      $mentorid=$this->input->post('mentorid');
       $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"1",'added_by'=>$mentorid,'user_type'=>"0"));
        if($query->num_rows() > 0){
          $arr=$query->result();
          $response['data']= $arr[0];
          $response['message']= "List of Mentor ";
          $response['status']= 1;
        }else{
          $response['data']= '';
          $response['message']="No mentor found ";
          $response['status']= 0;
        }
        $this->response($response, 200);
       //"activeMenteeList_Addebyself" => $this->model->fetch("*", 'tbl_mentor_mentee','status ="1" and user_type="1" and added_by="'.$user_id.'"')
    }
    //this function is used to return Unique id  after checking number
    function display_uni() {
    	$activelinkkey   = substr( rand() * 900000 + 100000, 0, 6 );
    	$query = $this->db->get_where('tbl_mentor_mentee',array('activelinkkey'=>$activelinkkey));
    	if($query->num_rows() > 0){
    			 display_uni();
    	}else{
    		return $activelinkkey;
    	}
    }

    //this is used to register mentee
    function menteeRegister_post(){
         $email=$this->input->post('email');
         $firstname=$this->input->post('firstname');
         $lastname=$this->input->post('lastname');
         $mentor_email=$this->input->post('mentor_email');
         $mentor_name=$this->input->post('mentor_name');
         $password=$this->input->post('password');
         $invitationId=$this->input->post('invitationId');
         $menteeRelationName=$this->input->post('menteeRelationName');

         $query = $this->db->get_where('user_management',array('email'=>$email));
          if($query->num_rows() > 0){

            $response['data']= $user;
            $response['message']= $email." is already associated with iGGi.";
            $response['status']= 0;
          }else{
            $data = array(
  					  'type'    => 'direct',
  		    		'email'   => $email,
  		    		'password'=> md5($password),
  		    		'status'  => 1,
  		    		'changed' => date('Y-m-d H:i:s'),
  		    		'created' => date('Y-m-d H:i:s'),
  						'first_name' => $firstname,
  						'last_name' => $lastname,
  						'user_type'=>1,
  						'mentor_type'=>$menteeRelationName,
  						'post_right'=>1
  		    	);
            $this->db->insert('user_management', $data);
  	    		$insert_id = $this->db->insert_id();


            $ida= $insert_id;
            $folder_user=FCPATH."uploads/user".$ida;
            mkdir($folder_user);
            $data_me = array(
                 'status' => '1',
                 'uid'=>$ida
              );
            $this->db->where('id',$invitationId);
            $this->db->update('tbl_mentor_mentee', $data_me);

            $this->load->library('SendEmail', null, 'mailObj');
              $strpEmailTo =$email;
              $strpRecipientName = '';
              $arrpTags = array();
              $arrpTags['{--RecipientName--}'] = $strpRecipientName;
              $arrpTags['{--SiteHomeURL--}'] = base_url();
              $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
              $arrpTags['{--EmailId--}'] =$email;
              $arrpTags['{--MyAuthPassword--}'] = $password;
              $arrpTags['{--Subject--}'] = "IGGI | Account Login Details";
              $arrpTags['{--LoginUrl--}'] = base_url();
             // $arrpTags['{--InvitedBy--}'] = '<a href="#" title="'.$usr_arr[0]->email.'">"'.$usr_arr[0]->first_name.'"</a>';

              $strpTempleteid = 5;
              if($this->mailObj->sendEmail(
                 $strpEmailTo,
                 $strpRecipientName,
                 $arrpTags,
                 $strpTempleteid,
                 $AdminEmailId = '',
                 $AdminName = '')) ;
              //-------------------------Another Email for mentor notifications---------------------------------\
              $this->load->library('SendEmail', null, 'mailObj');
              $strpEmailTo =$mentor_email;
              $strpRecipientName = '';
              $arrpTags = array();
              $arrpTags['{--RecipientName--}'] = $mentor_name;
              $arrpTags['{--SiteHomeURL--}'] = base_url();
              $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

              $arrpTags['{--Subject--}'] = "IGGI | Mentee Register notifications";
              $arrpTags['{--LoginUrl--}'] = base_url();
              $strpTempleteid = 11;
              if($this->mailObj->sendEmail(
                 $strpEmailTo,
                 $strpRecipientName,
                 $arrpTags,
                 $strpTempleteid,
                 $AdminEmailId = '',
                 $AdminName = '')) ;

          //  $query = $this->db->get_where('user_management',array('email'=>$email));
            //$user = $this->model->get("*", 'user_management', "id = ".$insert_id."");

            $response['data']= '';
            $response['message']= 'Mentee added successfully. ';
            $response['status']= 1;
          }
          $this->response($response, 200);

    }
    //this is used to get input  passcode and validated true and false
    function menteeRegisterCodeInput_post(){
           $txtInvitationCode=$this->input->post('passcode');
           $query = $this->db->get_where('tbl_mentor_mentee',array('activelinkkey'=>$txtInvitationCode,'status'=>'0'));
           $mentee_arr=$query->result();
             if($query->num_rows() > 0){
               $response['data']= $mentee_arr[0];
               $response['message']= 'verified';
               $response['status']= 1;
             }else{
               $response['data']= '';
               $response['message']= 'unverified';
               $response['status']= 0;
             }
             $this->response($response, 200);
      }
  //this function is used to invitation mentee and send code  for invitation
    function menteeInvitation_post(){
      $header_data=$this->input->request_headers();
      $token_withuserid=$header_data['iggi_token'];
      if($token_withuserid==""){
        $response['data']= '';
        $response['message']= 'unauthorise token!! OPPS';
        $response['status']= 0;
          $this->response($response, 200);

      }else{
      $token_arr = explode("18IGGI$", $token_withuserid);
      $user_id=$token_arr[1];
      $email= $this->post('txtInviteMentee');
    	$plan_feature_arr=getPlanFeatureByuserID($user_id);
    	$plan_featureUsed_arr=getPlanFeatureUsedByuserID($user_id);
    	if($plan_featureUsed_arr['mentee_number']>=$plan_feature_arr['mentee_number']){
        $response['data']= '';
        $response['message']= 'Upgrate Your plan . excced mentee invitation';
        $response['status']= 0;
    }else{
      $invite_code   =$this->display_uni();
      $usr_arr=getUserById($user_id);

    	$txtUserType_Invited='MentorInvited';
    	if($txtUserType_Invited=="MentorInvited"){
    		$usertypeid='1';
    		$query = $this->db->get_where('tbl_mentor_mentee',array('email'=>$email,'status'=>'1'));
    		if($query->num_rows() > 0){
          $response['data']= '';
          $response['message']= 'Already Member of IGGI';
          $response['status']= 0;
    		}else{
    		$this->load->library('encrypt');
    		$activelinkkey=$this->encrypt->encode($key_active);
    		$this->load->library('SendEmail', null, 'mailObj');
    		$strpEmailTo = $email;
    		$strpRecipientName = '';
    		$arrpTags = array();
    		$arrpTags['{--RecipientName--}'] = $strpRecipientName;
    		$arrpTags['{--SiteHomeURL--}'] = base_url();
    		$arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
    		$arrpTags['{--EmailId--}'] = $email;
    		$arrpTags['{--Subject--}'] = "Invitaion Code";
    		$arrpTags['{--LoginUrl--}'] = base_url();
    		$arrpTags['{--invitationcode--}'] =$invite_code;
    		$signup_link=BASE."index.php/mentee/signup_link/".$usr_arr[0]->id;

    		$arrpTags['{--InvitedBy--}'] = '<a href="'.$signup_link.'" title="'.$usr_arr[0]->email.'">"SignUp Now"</a>';
    		$strpTempleteid = 1;
    		if($this->mailObj->sendEmail(
    				$strpEmailTo,
    				$strpRecipientName,
    				$arrpTags,
    				$strpTempleteid,
    				$AdminEmailId = '',
    				$AdminName = '')) ;
    				$query = $this->db->get_where('tbl_mentor_mentee',array('email'=>$email,'added_by'=>$user_id));
    				if($query->num_rows() > 0){
    				//update code
    				$data = array(
    					'created_on' => NOW,
    					'user_type'=>$usertypeid,
    					'activelinkkey'=>$invite_code
    				);
    				$this->db->update('tbl_mentor_mentee', $data, array('added_by' => $user_id,'status'=>'0'));
    				}else{
    					//insert code
    					$data = array(
    				 		'user_type'=>$usertypeid,
    				 		'added_by' => $user_id,
    				 		'status' => '0' ,
    				 		'created_on' => date('Y-m-d h:i:s'),
    				 		'activelinkkey'=>$invite_code,
    				 		'email'=>$email
    				  );
    				 $lid= $this->db->insert('tbl_mentor_mentee', $data);

    					$sql = $this->db->last_query();


    				}
            $response['data']= '';
            $response['message']= 'Invitation sent successfully';
            $response['status']= 1;
    			}

    	}else{
    		echo $usertypeid="0";
    		$invite_code="";
    		$data = array(
    			'user_type'=>$usertypeid,
    			'added_by' => $user_id,
    			'status' => '0' ,
    			'created_on' => NOW,
    			'activelinkkey'=>$invite_code,
    			'email'=>$email
    		);
    	 $lid= $this->db->insert('tbl_mentor_mentee', $data);

    		$this->load->library('SendEmail', null, 'mailObj');
    		$strpEmailTo = $email;
    		$strpRecipientName = '';
    		$arrpTags = array();
    		$arrpTags['{--RecipientName--}'] = $strpRecipientName;
    		$arrpTags['{--SiteHomeURL--}'] = base_url();
    		$arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
    		$arrpTags['{--EmailId--}'] = $email;
    		$arrpTags['{--Subject--}'] = "invitation for Join IGGI APP As Mentor";
    		$arrpTags['{--LoginUrl--}'] = base_url();
    		$arrpTags['{--senderInfo--}'] =$usr_arr[0]->first_name."".$usr_arr[0]->last_name;
    		$arrpTags['{--InvitedBy--}'] = '<a href="#" title="'.$usr_arr[0]->email.'">"'.$usr_arr[0]->first_name.'"</a>';
    		$strpTempleteid = 10;
    		if($this->mailObj->sendEmail(
    				$strpEmailTo,
    				$strpRecipientName,
    				$arrpTags,
    				$strpTempleteid,
    				$AdminEmailId = '',
    				$AdminName = '')) ;


    	}
    	}
        }
        $this->response($response, 200);
    }

    /*
    |--------------------------------------------------------------------------
    | [3]. getForgetPassword_post |Start
    | URL: http://dev.iggiapp.com/index.php/Api/getUserDetails
    |--------------------------------------------------------------------------    |
    | INPUT : header iggi_token
    |
    */

    /*
    |--------------------------------------------------------------------------
    | [3]. getUserDetails_post |Start
    | URL: http://dev.iggiapp.com/index.php/Api/getUserDetails
    |--------------------------------------------------------------------------    |
    | INPUT : header iggi_token
    |
    */
    function getForgetPassword_post(){
       $email    = $this->post('email');

       $query = $this->db->get_where('user_management',array('email'=>$email));

        if($query->num_rows() > 0){
          function randomPassword() {
          $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
          $pass = array(); //remember to declare $pass as an array
          $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
          for ($i = 0; $i < 8; $i++) {
              $n = rand(0, $alphaLength);
              $pass[] = $alphabet[$n];
          }
          return implode($pass); //turn the array into a string
      }

                //----------------send forget password
                $txtForgetEmail=$email;
                $this->load->library('SendEmail', null, 'mailObj');
                $strpEmailTo = $txtForgetEmail;
                $strpRecipientName = '';
                $arrpTags = array();
                $txtPass=randomPassword();

                $data = array(
                     'password'=> md5($txtPass),

                  );

                $this->db->where('email', $txtForgetEmail);
                $this->db->update('user_management', $data);
                $arrpTags['{--SiteHomeURL--}'] = base_url();
                $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
                $arrpTags['{--EmailId--}'] = $strpEmailTo;
                $arrpTags['{--Subject--}'] = "Forget Password | IGGI ";
                $arrpTags['{--MyAuthPassword--}'] = $txtPass;

                $strpTempleteid = 3;

                if($this->mailObj->sendEmail(
                   $strpEmailTo,
                   $strpRecipientName,
                   $arrpTags,
                   $strpTempleteid,
                   $AdminEmailId = '',
                   $AdminName = '')) ;



          $response['data']= '';
          $response['message']= 'email sent successfully';
          $response['status']= 1;

        }else{
          $result_arr=$query->result();
          $response['data']= '';
          $response['message']= 'Invalid Credentials ';
          $response['status']= 0;
        }
        $this->response($response, 200);


    }


    function getUserDetails_post(){
      $header_data=$this->input->request_headers();
      $token_withuserid=$header_data['iggi_token'];
      $token_arr = explode("18IGGI$", $token_withuserid);
       $userid=$token_arr[1];
      $this->db->select('id,type,email,status,user_type,post_right,first_name,last_name,mentor_type,org_type,profile_pic,inbox_email_notify,post_content_email_notify,referal_point,mentor_type_other');
       $query = $this->db->get_where('user_management',array('user_access_token'=>$token_withuserid));
       $str = $this->db->last_query();
         if($query->num_rows() > 0){
             $result_arr=$query->result();
             $response['data']= $result_arr[0];
             $response['message']= 'get user details';
             $response['status']= 1;
         }else{
           $response['data']= '';
           $response['message']= 'OPPS! unauthorise access';
           $response['status']= 0;
         }
         $this->response($response, 200);


    }
    /*
    |--------------------------------------------------------------------------
    | [3]. User Profile Updataion |Start
    | URL: http://dev.iggiapp.com/index.php/Api/updateUserProfile
    |--------------------------------------------------------------------------    |
    | INPUT : required
    | 'first_name'   required
    | 'last_name'   required
    | 'status'  enum | [0,1]
    |
    */
    function updateUserProfile_post(){

       $user_id    = $this->post('user_id');
       $first_name    = $this->post('first_name');
       $last_name     = $this->post('last_name');
       $usertype     = $this->post('usertype');
       $mentor_type     = $this->post('mentor_type');
       $org_type     = $this->post('org_type');

       if($user_id==""){
         $response['data']='';
         $response['message']= "user_id is required";
         $response['status']= 0;
         $this->response($response, 200);
       }
       if($first_name==""){
         $response['data']='';
         $response['message']= "first_name is required";
         $response['status']= 0;
         $this->response($response, 200);
       }
       if($last_name==""){
         $response['data']='';
         $response['message']= "last_name is required";
         $response['status']= 0;
         $this->response($response, 200);
       }


       if($usertype==1){
         //mentee updatetion
         $data_update = array(
                    'first_name' => $first_name,
                    'last_name' =>$last_name,
                    'org_type'=>$org_type,
                    'mentor_type'=>$mentor_type
        );
     		$this->db->where('id', $user_id);
     		$this->db->update('user_management', $data_update);
        $response['data']= '';
        $response['message']= 'Profile Updated';
        $response['status']= 1;
       }else{
         //Mentor updatetion
         $data_update = array(
                    'first_name' => $first_name,
                    'last_name' =>$last_name,
                    'org_type'=>$org_type,
                    'mentor_type'=>$mentor_type
        );
     		$this->db->where('id', $user_id);
     		$this->db->update('user_management', $data_update);
        $response['data']= '';
        $response['message']= 'Profile Updated';
        $response['status']= 1;
       }

        $this->response($response, 200);
    }
    /*
    |--------------------------------------------------------------------------
    | [3]. User Profile Updataion |STOP
    | URL: http://dev.iggiapp.com/index.php/Api/updateUserProfile
    |--------------------------------------------------------------------------    |
    | INPUT : required
    | 'first_name'   required
    | 'last_name'   required
    | 'status'  enum | [0,1]
    |
    */

    /*
    |--------------------------------------------------------------------------
    | [1]. Mentor Register API |Start
    | URL: http://dev.iggiapp.com/index.php/Api/mentorRegister
    |--------------------------------------------------------------------------    |
    | INPUT : required
    | 'email'   required
    | 'password'  required | with six character
    |
    */

    function mentorRegister_post(){
       $email      = $this->post('email');
       $password     = $this->post('password');
       $query = $this->db->get_where('user_management',array('email'=>$email));

        if($query->num_rows() > 0){
          $response['data']= '';
          $response['message']= 'This email is already associated with IGGI APP';
          $response['status']= 0;


        }else{
          $data = array(
  					  'fullname'=> '',
  					  'type'    => 'mobileAPP',
  		    		'email'   => $email,
  		    		'password'=> md5($password),
  		    		'changed' => Date('Y-m-d H:i:s'),
  		    		'created' => Date('Y-m-d H:i:s'),
  						'user_type'=>'0',
  						'post_right'=>'0'
  		    	);
       $id = $this->Iggi_model->RegisterMentor($data);
			$insert_id = $this->db->insert_id();
       $user_arr['userid']=$insert_id;
			 $response['data']= $user_arr;
          $response['message']= 'get mentor details';
          $response['status']= 1;

        }

     $this->response($response, 200);


    }
    /*
    |--------------------------------------------------------------------------
    | [1]. Mentor Register API |End
    /--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | [2]. Login in IGGI API |Start
    | URL: http://dev.iggiapp.com/index.php/Api/userLogin
    |--------------------------------------------------------------------------    |
    | INPUT : required
    | 'email'   required
    | 'password'  required | with six character
    |
    */

    function userLogin_post(){
        $email      = $this->post('email');
        $password     = $this->post('password');
        $device_id     = $this->post('device_id');
        $device_type     = $this->post('device_type');
        $device_name     = $this->post('device_name');
        $query = $this->db->get_where('user_management',array('email'=>$email,'password'=>md5($password)));
        $result_arr=$query->result();

        if($query->num_rows() > 0){
          //update device id
          $char = "bcdfghjkmnpqrstvzBCDFGHJKLMNPQRSTVWXZaeiouyAEIOUY";
          $token = '';
          for ($i = 0; $i < 40; $i++) $token .= $char[(rand() % strlen($char))];
          if($device_type==0){
            $data_token = array(
                 'user_access_token' => $token."18IGGI$".$result_arr[0]->id,
                 'iphone_devive_id'=>$device_id,
                 'ios_device_name'=>$device_name
            );
          }else{
            $data_token = array(
                 'user_access_token' => $token."18IGGI$".$result_arr[0]->id,
                 'android_devive_id'=>$device_id,
                 'android_device_name'=>$device_name
            );
          }

          $uid=$result_arr[0]->id;
          $this->db->where('id', $uid);
          $this->db->update('user_management', $data_token);


          $query = $this->db->get_where('user_management',array('email'=>$email,'password'=>md5($password)));
          $result_arr_new=$query->result();
          $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
          $pro_phto=$result_arr_new[0]->profile_pic;
          if(empty($pro_phto)){
             $pro_img_url=BASE."uploads/profile/avator-default.png";
          }else{
            if($pro_pic_arr->pic_from=='self'){
              $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
            }else{
              $pro_img_url= $result_arr_new[0]->profile_pic;
            }
          }


          $usr_arr = array(
            'id' =>intval($result_arr_new[0]->id),
            'ios_access' =>intval($result_arr_new[0]->ios_access),
            'admin' =>intval($result_arr_new[0]->admin),
            'type' =>$result_arr_new[0]->type,
            'pid' =>$result_arr_new[0]->pid,
            'fullname'=>is_null($result_arr_new[0]->fullname) ? '':$result_arr_new[0]->fullname,
            'email'=>is_null($result_arr_new[0]->email) ? '':$result_arr_new[0]->email,
            'user_type'=>is_null($result_arr_new[0]->user_type) ? '':intval($result_arr_new[0]->user_type),
            'first_name'=>is_null($result_arr_new[0]->first_name) ? '':$result_arr_new[0]->first_name,
            'last_name'=>is_null($result_arr_new[0]->last_name) ? '':$result_arr_new[0]->last_name,
            'user_access_token'=>is_null($result_arr_new[0]->user_access_token) ? '':$result_arr_new[0]->user_access_token,
            'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
            'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
            'profile_pic'=>is_null($pro_img_url) ? '':$pro_img_url,
            'android_devive_id'=>is_null($result_arr_new[0]->android_devive_id) ? '':$result_arr_new[0]->android_devive_id,
            'iphone_devive_id'=>is_null($result_arr_new[0]->iphone_devive_id) ? '':$result_arr_new[0]->iphone_devive_id,






          );




          $response['data']= $usr_arr;
          $response['message']= 'get user details';
          $response['status']= 1;


        }else{
          $response['data']= '';
          $response['message']= 'Incorrect Credentials';
          $response['status']= 0;
        }
        $this->response($response, 200);

    }
    /*
    |--------------------------------------------------------------------------
    | [2]. Mentor Register API |End
    /--------------------------------------------------------------------------
    */

    //API -  Fetch All books

    function booksAget_get(){

      $result = $this->Iggi_model->getallbooks();

     if($result){

         $this->response($result, 200);

     }

     else{

         $this->response("No record found", 404);

     }
    }
    //this api is used to upload images as base64 and
    function uploadIMG_post(){
      // $header_data=$this->input->request_headers();
      // $token_withuserid=$header_data['iggi_token'];
      // if($token_withuserid==""){
      //   $response['data']= '';
      //   $response['message']= 'unauthorise token!! OPPS';
      //   $response['status']= 0;
      //     $this->response($response, 200);
      //
      // }else{
      // $token_arr = explode("18IGGI$", $token_withuserid);
      // $user_id=$token_arr[1];
      // }

      $img_base64      = $this->post('img_base64');
      $user_id      = $this->post('userid');
      define('UPLOAD_DIR', './uploads/profile/');
      $img = str_replace('data:image/jpeg;base64,', '', $img_base64);
      $data = base64_decode($img);
      $file =  UPLOAD_DIR.get_UNI().date(hmiymd).'.png';
      $success = file_put_contents($file, $data);
      $data1[] = explode('./uploads/profile/',$file);
      $img_name=$data1[0][1];

      $arrayPicInfo = array(
        'pic_from' => 'self',
        'pic_name_url' => $img_name,

       );
      $data_pro = array(
                 'profile_pic' => json_encode($arrayPicInfo)
              );
       $this->db->where('id', $user_id);
      $this->db->update('user_management', $data_pro);
      $locationimg=BASE."uploads/profile/".$img_name;
      $response['data']= $locationimg;
      $response['message']= 'profile url';
      $response['status']= 1;
      $this->response($response, 200);

    }

    /*
    | -------------------------------------------------------------------------
    |  login : base_url+revokeIOS
    | -------------------------------------------------------------------------
    */
    function revokeIOS_post(){
        $user_id      = $this->post('userid');
        $data_pro = array(
                   'iphone_devive_id' => ""
                );
        $this->db->where('id', $user_id);
        $this->db->update('user_management', $data_pro);
        $response['data']= '';
        $response['message']= 'Revoke access of ios successfully';
        $response['status']= 1;
        $this->response($response, 200);
    }


    /*
    | -------------------------------------------------------------------------
    |  login : base_url+revokeIOS
    | -------------------------------------------------------------------------
    */
    function revokeAndroid_post(){
        $user_id      = $this->post('userid');
        $data_pro = array(
                   'android_devive_id' => ""
                );
        $this->db->where('id', $user_id);
        $this->db->update('user_management', $data_pro);
        $response['data']= '';
        $response['message']= 'Revoke access of Android successfully';
        $response['status']= 1;
        $this->response($response, 200);
    }


}
//end of file
