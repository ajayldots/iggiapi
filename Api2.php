<?php
require(APPPATH.'/libraries/REST_Controller.php');
class Api2 extends REST_Controller{
    public function __construct()    {
        parent::__construct();
        $this->load->model('Iggi_model');
        $this->load->database();
        	$this->load->library('MailChimp');
    }

    //this function is used to return Unique id  after checking number
    function display_uni() {
    	$activelinkkey   = substr( rand() * 900000 + 100000, 0, 6 );
    	$query = $this->db->get_where('tbl_mentor_mentee',array('activelinkkey'=>$activelinkkey));
    	if($query->num_rows() > 0){
    			 display_uni();
    	}else{
    		return $activelinkkey;
    	}
    }

    /*
    | -------------------------------------------------------------------------
    |  login : base_url+userLogin
    | -------------------------------------------------------------------------
    */
    function userLogin_post(){
        $email      = $this->post('email');
        $password     = $this->post('password');
        $device_id     = $this->post('device_id');
        $device_type     = $this->post('device_type');
        $device_name     = $this->post('device_name');

        $query = $this->db->get_where('user_management',array('email'=>$email,'password'=>md5($password)));
        $result_arr=$query->result();
        if($query->num_rows() > 0){
          $char = "bcdfghjkmnpqrstvzBCDFGHJKLMNPQRSTVWXZaeiouyAEIOUY";
          $token = '';
          for ($i = 0; $i < 40; $i++) $token .= $char[(rand() % strlen($char))];
              if($device_type==0){
                $data_token = array(
                     'user_access_token' => $token."18IGGI$".$result_arr[0]->id,
                     'iphone_devive_id'=>$device_id,
                     'ios_device_name'=>$device_name
                );
              }else{
                $data_token = array(
                     'user_access_token' => $token."18IGGI$".$result_arr[0]->id,
                     'android_devive_id'=>$device_id,
                     'android_device_name'=>$device_name
                );
              }
          $uid=$result_arr[0]->id;
          $this->db->where('id', $uid);
          $this->db->update('user_management', $data_token);
          $query = $this->db->get_where('user_management',array('email'=>$email,'password'=>md5($password)));
          $result_arr_new=$query->result();
          $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
          $pro_phto=$result_arr_new[0]->profile_pic;
          if(empty($pro_phto)){
             $pro_img_url=BASE."uploads/profile/avator-default.png";
          }else{
            if($pro_pic_arr->pic_from=='self'){
              $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
            }else{
              $pro_img_url= $result_arr_new[0]->profile_pic;
            }
          }
          if(empty($result_arr_new[0]->iphone_devive_id)){
            $ios_device_status=0;
          }else{
            $ios_device_status=1;
          }
          if(empty($result_arr_new[0]->android_devive_id)){
            $android_devive_id_status=0;
          }else{
            $android_devive_id_status=1;
          }
          $user_arr=array(
            'email'=>$result_arr_new[0]->email,
            'status'=>$result_arr_new[0]->status,
            'user_type'=>intval($result_arr_new[0]->user_type),
            'post_right'=>intval($result_arr_new[0]->post_right),
            'first_name'=>$result_arr_new[0]->first_name,
            'last_name'=>$result_arr_new[0]->last_name,
            'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
            'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
            'profile_pic'=>$pro_img_url,
            'mentor_type_other'=>is_null($result_arr_new[0]->mentor_type_other) ? '':$result_arr_new[0]->mentor_type_other,
            'user_access_token'=>$result_arr_new[0]->user_access_token,
            'ios_device_status'=>$ios_device_status,
            'android_devive_id_status'=>$android_devive_id_status,
            'profile_complete_check'=>$result_arr_new[0]->first_name="" ? 0:1

          );
		  if(intval($result_arr_new[0]->user_type)){
			  $subscrption_arr=get_subscribedplanByuserID($uid);
				$arrLoginResponse =array(
				'userinfo' =>$user_arr,
				'subscription'=>(object)array()
			  );
		  }else{
			  $arrLoginResponse =array(
				'userinfo' =>$user_arr,
				'subscription'=>$subscrption_arr[0]
			  );
		  }

          $response['data']= $arrLoginResponse;
          $response['message']= 'get user details';
          $response['status']= 1;
        }else{
          $response['data']= (object)array();
          $response['message']= 'Incorrect Credentials';
          $response['status']= 0;
        }
        $this->response($response, 200);

    }
    /*
    | -------------------------------------------------------------------------
    |  login : base_url+profileImgUpload
    | -------------------------------------------------------------------------
    */

    function profileImgUpload_post(){
      $header_data=$this->input->request_headers();
      $token_withuserid=$header_data['iggi_token'];
      $token_arr = explode("18IGGI$", $token_withuserid);

      $img_base64      = $this->post('img_base64');
      if($img_base64==""){
        $response['data']= '';
        $response['message']= 'need base64 data';
        $response['status']= 0;
      }
      $user_id      = $token_arr[1];
      define('UPLOAD_DIR', './uploads/profile/');
      $img = str_replace('data:image/jpeg;base64,', '', $img_base64);
      $data = base64_decode($img);
      $file =  UPLOAD_DIR.get_UNI().date(hmiymd).'.png';
      $success = file_put_contents($file, $data);
      $data1[] = explode('./uploads/profile/',$file);
      $img_name=$data1[0][1];
      $arrayPicInfo = array(
        'pic_from' => 'self',
        'pic_name_url' => $img_name,

       );
      $data_pro = array(
                 'profile_pic' => json_encode($arrayPicInfo)
              );
      $this->db->where('id', $user_id);
      $this->db->update('user_management', $data_pro);
      $locationimg=BASE."uploads/profile/".$img_name;
      $response['data']= $locationimg;
      $response['message']= 'profile url';
      $response['status']= 1;
      $this->response($response, 200);

    }
    /*
    | -------------------------------------------------------------------------
    |  login : base_url+revokeIOS
    | -------------------------------------------------------------------------
    */
    function revokeIOS_post(){
        $user_id      = $this->post('userid');
        $data_pro = array(
                   'iphone_devive_id' => ""
                );
        $this->db->where('id', $user_id);
        $this->db->update('user_management', $data_pro);
        $response['data']= '';
        $response['message']= 'Revoke access of ios successfully';
        $response['status']= 1;
        $this->response($response, 200);
    }


    /*
    | -------------------------------------------------------------------------
    |  login : base_url+revokeIOS
    | -------------------------------------------------------------------------
    */
    function revokeAndroid_post(){
        $user_id      = $this->post('userid');
        $data_pro = array(
                   'android_devive_id' => ""
                );
        $this->db->where('id', $user_id);
        $this->db->update('user_management', $data_pro);
        $response['data']= '';
        $response['message']= 'Revoke access of Android successfully';
        $response['status']= 1;
        $this->response($response, 200);
    }

        /*
        | -------------------------------------------------------------------------
        |  login : base_url+getPendingPost
        | -------------------------------------------------------------------------
        */
        function getPendingPost_post(){
          $userid=$this->input->post('userid');
          $limit = $this->post('limit');
          $offset = $this->post('offset');
          $this->db->where('uid', $userid);
          $this->db->where('status', 0);
          $results = $this->db->get('tbl_mymentor_posts', $limit, $offset)->result();

          $arr_pending_post = array();
          foreach ($results as $key => $value) {
            $arr_pending_post[] = array(
              'id'=>intval($value->id),
              'post_title'=>$value->post_title,
              'social_media_type'=>$value->social_media_type,
              'type'=>$value->type,
              'socila_username'=>$value->name,
              'message'=>$value->message,
              'title'=>is_null($value->title) ? '':$value->title,
              'description'=>is_null($value->description) ? '':$value->description,
              'url'=>is_null($value->url) ? '':$value->url,
              'image'=>is_null($value->image) ? '':$value->image,
              'video_url'=>is_null($value->video_url) ? '':$value->video_url,
              'time_post'=>$value->time_post,
              'status'=>intval($value->status),
              'approved_by'=>intval($value->approved_by)
            );
          }



          if(count($results) > 0){

          $response['data']=$arr_pending_post;
          $response['message']= "list of all pending posts";
          $response['status']= 1;
        }else{
          $response['data']='';
          $response['message']= "no record found!";
          $response['status']= 0;
        }

       $this->response($response, 200);

        }

        /*
        | -------------------------------------------------------------------------
        |  login : base_url+getAllpost
        | -------------------------------------------------------------------------
        */
          function getAllpost_post(){
            $userid=$this->input->post('userid');
            $limit = $this->post('limit');
            $offset = $this->post('offset');
            $this->db->where('uid', $userid);
            $results = $this->db->get('tbl_mymentor_posts', $limit, $offset)->result();
            //-----------------------------get pending post----------------------------
            $this->db->where('uid', $userid);
            $this->db->where('status', 0);
            $results_pending = $this->db->get('tbl_mymentor_posts')->result();
            //-------------------------------------------------------------------------

            $arr_pending_post = array();
            foreach ($results as $key => $value) {
              $arr_pending_post[] = array(
                'id'=>intval($value->id),
                'post_title'=>$value->post_title,
                'social_media_type'=>$value->social_media_type,
                'type'=>$value->type,
                'socila_username'=>$value->name,
                'message'=>$value->message,
                'title'=>is_null($value->title) ? '':$value->title,
                'description'=>is_null($value->description) ? '':$value->description,
                'url'=>is_null($value->url) ? '':$value->url,
                'image'=>is_null($value->image) ? '':$value->image,
                'video_url'=>is_null($value->video_url) ? '':$value->video_url,
                'time_post'=>$value->time_post,
                'status'=>intval($value->status),
                'approved_by'=>intval($value->approved_by)
              );
            }

            if(count($results) > 0){

            $response['data']=$arr_pending_post;
            $response['pendingpost']=count($results_pending);
            $response['message']= "list of all pending posts";
            $response['status']= 1;
          }else{
            $response['data']='';
            $response['message']= "no record found!";
            $response['status']= 0;
          }
         $this->response($response, 200);


          }

          /*
          | -------------------------------------------------------------------------
          |  login : base_url+getSocialMediaFeed
          | -------------------------------------------------------------------------
          */
          function getSocialMediaFeed_post(){
            $social_feeed=$this->input->post('social_media_name');
            $limit = $this->post('limit');
            $offset = $this->post('offset');
            $txtUserID=$this->input->post('userid');
            //getting twitter feed --
            if($social_feeed=="twitter"){
            $twitter_arr=getTwitterUser($txtUserID);
        		$twitter_arr = array_filter($twitter_arr);
            $arr_social_feed= array();
        		 if (!empty($twitter_arr)) {
        		       $arr_social_data=get_tweeterfeed_byID($txtUserID);
        		       foreach ($arr_social_data as $key => $value) {
                     $arr_social_feed[]=array(
                       'profile_picture' =>is_null($profile_img=$value->user->profile_background_image_url_https) ? '':$profile_img=$value->user->profile_background_image_url_https,
                       'user_name' =>is_null($value->user->name) ? '':$value->user->name,
                       'user_link' =>is_null($value->user->url) ? '':$value->user->url,
                       'created_at' =>is_null($value->created_at) ? '':$value->created_at,
                       'text' => is_null( $value->text) ? '': $value->text,
                       'picture' => is_null($value->picture) ? '': $value->picture,
                       'retweet_count' =>is_null($value->retweet_count) ? 0: $value->retweet_count ,
                       'favorite_count' =>is_null($value->favorite_count) ? 0: $value->favorite_count
                     );
                   }
                   $response['data']=$arr_social_feed;
                   $response['message']= "Twitter Feeds";
                   $response['status']= 1;
                   $this->response($response, 200);
            }else{
              //no feed available return empty array
              $response['data']=array();
              $response['message']= "No feeds available";
              $response['status']= 0;
              $this->response($response, 200);
            }
          }
            //getting twitter feed ---
            //getting google plus feed --
            if($social_feeed=="google_plus_feed"){
          		$gooleplus_arr=getGooglePLUSUser($txtUserID);
          		$gooleplus_arr = array_filter($gooleplus_arr);
          		if (!empty($gooleplus_arr)) {
          		$redirect_uri=PATH."gplus";
          		require_once APPPATH.'libraries/google_auth/vendor/autoload.php';
          		$client = new Google_Client();
          		$client->setClientId(GOOGLE_ID);
          		$client->setClientSecret(GOOGLE_SECRET);
          		$client->setRedirectUri($redirect_uri);
          		$client->setAccessType("offline");
          		$client->setApprovalPrompt("force");
          		$client->setScopes(array(
          		'https://www.googleapis.com/auth/plus.circles.read',
          		'https://www.googleapis.com/auth/plus.circles.write',
          		'https://www.googleapis.com/auth/plus.me',
          		'https://www.googleapis.com/auth/plus.media.upload',
          		'https://www.googleapis.com/auth/plus.stream.read',
          		'https://www.googleapis.com/auth/plus.stream.write'
          		));

          		$gooleplus_arr=getGooglePLUSUser($txtUserID);

          		$gooleplus_arr = array_filter($gooleplus_arr);
          		if (!empty($gooleplus_arr)) {
          		 $user_id=$gooleplus_arr[0]->user_id;
          		 $accesstoken=$gooleplus_arr[0]->refresh_token;
          		}

          		$_tokenArray['access_token'] = $accesstoken;
          		$client->setAccessToken( json_encode($_tokenArray));
          		//check if token expired:
          		if ( $client->isAccessTokenExpired() ) {
          				$client->refreshToken($accesstoken);
          				$new_access_token = $client->getAccessToken();
          			}
          		 $newliveaccess_token=$new_access_token['access_token'];

          		 $url = 'https://www.googleapis.com/plusDomains/v1/people/' . $user_id . '/activities/user';
          		 $headers = array(
          				 'Authorization :  Bearer ' . $newliveaccess_token,
          				 'Content-Type : application/json',
          		 );
          		 $ch = curl_init();
          		 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          		 curl_setopt($ch, CURLOPT_HEADER, false);
          		 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          		 curl_setopt($ch, CURLOPT_URL, $url);
          		 $json_url = curl_exec($ch);
          		 $json_output = json_decode($json_url);
          		 curl_close($ch);
          		 $google_feed_arr=$json_output->items;
                $arr_social_feed= array();
          		 foreach ($google_feed_arr as $key => $value) {
                 $arr_social_feed[]=array(
                   'profile_picture' =>is_null($value->actor->image->url) ? '':$value->actor->image->url,
                   'user_name' =>is_null($value->actor->displayName) ? '':$value->actor->displayName,
                   'user_link' =>is_null($value->user->url) ? '':$value->user->url,
                   'created_at' =>is_null($value->created_at) ? '':$value->created_at,
                   'text' => is_null($value->title) ? '': $value->title,
                   'picture' => is_null($value->picture) ? '': $value->picture,
                   'replay' =>is_null($value->object->replies->totalItems) ? 0: $value->object->replies->totalItems ,
                   'plusoners' =>is_null($value->object->plusoners->totalItems) ? 0: $value->object->plusoners->totalItems,
                   'resharers' =>is_null($value->object->resharers->totalItems) ? 0: $value->object->resharers->totalItems

                 );
               }
               $response['data']=$arr_social_feed;
               $response['message']= "Google Feeds";
               $response['status']= 1;
               $this->response($response, 200);
             }else{
               $response['data']=array();
               $response['message']= "No feeds of google plus";
               $response['status']= 1;
               $this->response($response, 200);
             }
           }
            //getting google plus feed ---
            //getting instagram feed --
            	if($social_feeed=="instagram"){

                $instagram_arr=getInstagramUser($txtUserID);
          			$instagram_arr = array_filter($instagram_arr);
          			if (!empty($instagram_arr)) {

          			$arr_social_data=get_InstagramFeed_byID($txtUserID);
                   $arr_social_feed= array();

          			foreach ($arr_social_data->data as $key => $value) {
                    $img_data=$value->images->standard_resolution->url;
          				  $caption=$value->caption->text;
          					$created_time=$value->caption->created_time;
          					$link=$value->link;
          					$likes=$value->likes->count;
          					$comments=$value->comments->count;
          					$date_time_ago= date('Y-m-d H:i:s', $created_time);
                    $value->user->profile_picture;
                    $instagram_link="https://instagram.com/".$value->user->username;
                    $arr_social_feed[]=array(
                    'profile_picture' =>is_null($value->user->profile_picture) ? '':$value->user->profile_picture,
                    'user_name' =>is_null($value->actor->displayName) ? '':$value->actor->displayName,
                    'user_link' =>is_null($value->user->username) ? '':$instagram_link,
                    'created_at' =>is_null($date_time_ago) ? '':ucwords(time_ago($date_time_ago)),
                    'text' => is_null($caption) ? '': $caption,
                    'picture' => is_null($img_data) ? '': $img_data,
                    'comments' =>is_null($comments) ? 0: $comments ,
                    'likes' =>is_null($likes) ? 0: $likes
                  );

                  }
                  $response['data']=$arr_social_feed;
                  $response['message']= "Instagram feeds list";
                  $response['status']= 1;
                  $this->response($response, 200);
              }else{
                  $response['data']=array();
                  $response['message']= "No feeds of instagram available";
                  $response['status']= 1;
                  $this->response($response, 200);
              }
            }
            //getting instagram feed ---


          }
          /*
          | -------------------------------------------------------------------------
          |  login : base_url+postApproved
          | -------------------------------------------------------------------------
          */

          public function postApproved_post(){
          $id=$this->input->post('approvedid');
          $post_info_arr=get_SocailPostInformation_postID($id);
          $social_post_title=$post_info_arr[0]->post_title;
          $social_mentor_name=$this->input->post('txtMentornamePostApproval');
          $post_link=BASE."index.php/mentee/posts";
          $post_userid=$post_info_arr[0]->uid;
          $user_post_arr=getUserById($post_userid);
          $mentee_emailid=$user_post_arr[0]->email;
          $mentee_posts_approved=$this->input->post('mentee_posts_approved');
          $user_id=$user_post_arr[0]->id."-";
          $SQL="update tbl_mymentor_posts set status=1,changed='".date('Y-m-d h:i:s')."', approved_by = CONCAT(approved_by, '$user_id') where id = '$id'";
          $query = $this->db->query($SQL);
          $approve_post_arr=get_all_posts_approved_byid($id);
          $iggi_user_arr=getUserById($approve_post_arr[0]->uid);
          $buddyEmail=$iggi_user_arr[0]->email;
          $user_arr_logged=getUserById($user_post_arr[0]->id);
          $user_arr_logged[0]->post_content_email_notify;
          if($user_arr_logged[0]->post_content_email_notify=='1'){
            $this->load->library('SendEmail', null, 'mailObj');
            $strpEmailTo = $buddyEmail;
            $strpRecipientName = '';
            $arrpTags = array();
            $arrpTags['{--SiteHomeURL--}'] = base_url();
            $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
            $arrpTags['{--Subject--}'] = "IGGI Invitation AAA";
            $arrpTags['{--chatMessage--}'] = $message;
            $strpTempleteid = 8;

            if($this->mailObj->sendEmail(
               $strpEmailTo,
               $strpRecipientName,
               $arrpTags,
               $strpTempleteid,
               $AdminEmailId = '',
               $AdminName = '')) ;

              //--------------------------------------------------------------------------

          }



          $social_type=$approve_post_arr[0]->social_media_type;

          //----------google plus post----------------------------------------------------------------

          if($social_type=='google-plus-square'){
            $user_id=$approve_post_arr[0]->uid;
            $this->db->select("*");
            $this->db->from('tbl_google_plus');
            $this->db->where('uid', $user_id);
            $query = $this->db->get();
            $result_array = $query->result_array();
            foreach ($result_array as $key => $value) {
               $accesstoken=$value['refresh_token'];
               $user_id=$value['user_id'];
            }

            ini_set('display_errors', 0);
            error_reporting(E_ALL);
            $redirect_uri=PATH."mentee/post/approved";
            require_once APPPATH.'libraries/google_auth/vendor/autoload.php';
            $client = new Google_Client();
            $client->setClientId(GOOGLE_ID);
            $client->setClientSecret(GOOGLE_SECRET);
            $client->setRedirectUri($redirect_uri);
            $client->setAccessType("offline");
            $client->setApprovalPrompt("force");
            $client->setScopes(array(
            'https://www.googleapis.com/auth/plus.circles.read',
            'https://www.googleapis.com/auth/plus.circles.write',
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/plus.media.upload',
            'https://www.googleapis.com/auth/plus.stream.read',
            'https://www.googleapis.com/auth/plus.stream.write'
            ));

            $_tokenArray['access_token'] = $accesstoken;
            $client->setAccessToken( json_encode($_tokenArray));
            //check if token expired:
            if ( $client->isAccessTokenExpired() ) {
                $client->refreshToken($accesstoken);
                $new_access_token = $client->getAccessToken();
                }
             $newliveaccess_token=$new_access_token['access_token'];

             $url = 'https://www.googleapis.com/plusDomains/v1/people/' . $user_id . '/activities';
             $headers = array(
                 'Authorization : Bearer ' . $newliveaccess_token,
                 'Content-Type : application/json',
             );
             $post_data = array("object" => array("originalContent" => $approve_post_arr[0]->message), "access" => array("items" => array(array("type" => "domain")),"domainRestricted" => true));
             $data_string = json_encode($post_data);
             $ch = curl_init();
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
             curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
             curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             curl_setopt($ch, CURLOPT_URL, $url);
             $file_result = curl_exec($ch);
             curl_close($ch);

           $result_arr=json_decode($file_result);

          if (array_key_exists("error",$result_arr))
            {
              $SQL="update tbl_mymentor_posts set status=0,changed='".date('Y-m-d h:i:s')."', approved_by = CONCAT(approved_by, '$user_id') where id = '$id'";
              $query = $this->db->query($SQL);
              ms(array(
             "account-type"=> "google",
             "st"    => "error",
             "label" => "bg-red",
             "txt"   => "<span class='col-white'> Need Google's <a href='https://gsuite.google.com/''>G-Suite </a>account  </a></span>"
            ));

            }
          else {
                $data_post = array(
                   'time_post'=>date('Y-m-d H:i:s'),
                   'post_response_id' => $result_arr->id
                );
              $this->db->where('id', $id);
              $this->db->update('tbl_mymentor_posts', $data_post);
              //-----------start--------------Another Email for mentor notifications---------------------------------\
            $post_info_arr=get_SocailPostInformation_postID($id);
            $social_post_title=$post_info_arr[0]->post_title;
            $social_mentor_name=$this->input->post('txtMentornamePostApproval');
            $post_link=BASE."index.php/posts/view/".$id;
            $post_userid=$post_info_arr[0]->uid;
            $user_post_arr=getUserById($post_userid);
            $mentee_emailid=$user_post_arr[0]->email;
            date_default_timezone_set('UTC');
            $this->load->library('SendEmail', null, 'mailObj');
            $strpEmailTo =$mentee_emailid;
            $strpRecipientName = '';
            $arrpTags = array();
            $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
            $arrpTags['{--PostTitle--}'] = $social_post_title;
            $arrpTags['{--PostLink--}'] = $post_link;
            $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
            $arrpTags['{--SocialPlatform--}'] = 'Google Plus';
            $arrpTags['{--SiteHomeURL--}'] = base_url();
            $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

            $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
            $arrpTags['{--LoginUrl--}'] = base_url();
            $strpTempleteid = 12;
            if($this->mailObj->sendEmail(
               $strpEmailTo,
               $strpRecipientName,
               $arrpTags,
               $strpTempleteid,
               $AdminEmailId = '',
               $AdminName = '')) ;
            //---------Stop-----------------------------------------------------------

              ms(array(
             "st"    => "success",
             "label" => "bg-green",
             "txt"   => "<span class='col-white'>Google Post successfully posted! </a></span>"
            ));

            }

           //echo $code=$result_arr['code'];







          }
          //--------------------------------------------------------------------------


          if($social_type=='facebook'){
            $user_id=$approve_post_arr[0]->uid;
            $this->db->select("*");
            $this->db->from(FACEBOOK_ACCOUNTS);
            $this->db->where('uid', $user_id);
            $query = $this->db->get();
            $result_array = $query->result_array();
            foreach ($result_array as $key => $value) {
              $access_t=$value['access_token'];
            }

            $linkData = [
            'name'=>$approve_post_arr[0]->name,
            'link' =>$approve_post_arr[0]->url,
            'description'=>$approve_post_arr[0]->description,
            'message' => $approve_post_arr[0]->message,
            'picture' => '',
            'caption' => $approve_post_arr[0]->caption
            ];
            $FB = FbOAuth();
            $helper = $FB->getRedirectLoginHelper();
            $accessToken = $helper->getAccessToken();
            $client = $FB->getOAuth2Client();
            try {
              // Returns a long-lived access token
              $accessTokenLong = $client->getLongLivedAccessToken($access_t);

            } catch(Facebook\Exceptions\FacebookSDKException $e) {
              // There was an error communicating with Graph
              echo $e->getMessage();
              exit;
            }

            $post_type=$approve_post_arr[0]->type;
            if($post_type=='text' || $post_type=='link'){

                try {
                  // Returns a `Facebook\FacebookResponse` object
                  $response = $FB->post('/me/feed', $linkData,$accessTokenLong);
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                  $ErrorCodeA=$e->getCode();
                  if($ErrorCodeA==506){

                    $data_post = array(
                         'status' => 0
                      );
                    $this->db->where('id', $id);
                    $this->db->update('tbl_mymentor_posts', $data_post);

                    ms(array(
                   "st"    => "success",
                   "label" => "bg-red",
                   "txt"   => "<span class='col-white'>Facebook Duplicate message not allowed (You can approved after sometime)</a></span>"
                 ));
                  }
                  exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                  echo 'Facebook SDK returned an error: ' . $e->getMessage();
                  exit;
                }

                $user = $response->getGraphUser();
                $obj_id=$user['id'];
                //--------update tbl_mymentor_posts---------------------------------
                 $post_response_id = json_encode(array('object_id' =>$obj_id,'access_token'=>$accessTokenLong));
                 $data_post = array(
                      'post_response_id' => $post_response_id
                   );
                 $this->db->where('id', $id);
                 $this->db->update('tbl_mymentor_posts', $data_post);
                 //--------update tbl_mymentor_posts--------------------
                 //send email for post approvel

                 //-----------start--------------Another Email for mentor notifications---------------------------------\
                 $post_info_arr=get_SocailPostInformation_postID($id);
                 $social_post_title=$post_info_arr[0]->post_title;
                 $social_mentor_name=$this->input->post('txtMentornamePostApproval');
                 $post_link=BASE."index.php/posts/view/".$id;
                 $post_userid=$post_info_arr[0]->uid;
                 $user_post_arr=getUserById($post_userid);
                 $mentee_emailid=$user_post_arr[0]->email;
                 date_default_timezone_set('UTC');
                 $this->load->library('SendEmail', null, 'mailObj');
                 $strpEmailTo =$mentee_emailid;
                 $strpRecipientName = '';
                 $arrpTags = array();
                 $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
                 $arrpTags['{--PostTitle--}'] = $social_post_title;
                 $arrpTags['{--PostLink--}'] = $post_link;
                 $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
                 $arrpTags['{--SocialPlatform--}'] = 'Facebook';
                 $arrpTags['{--SiteHomeURL--}'] = base_url();
                 $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                 $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
                 $arrpTags['{--LoginUrl--}'] = base_url();
                 $strpTempleteid = 12;
                 if($this->mailObj->sendEmail(
                    $strpEmailTo,
                    $strpRecipientName,
                    $arrpTags,
                    $strpTempleteid,
                    $AdminEmailId = '',
                    $AdminName = '')) ;
                 //---------Stop-----------------------------------------------------------


                 ms(array(
                 "st"    => "success",
                 "label" => "bg-green",
                 "txt"   => "<span class='col-white'>Post Successfully posted </a></span>"
               ));


            } //end of text post of facebook


            if($post_type=="image"){
              $linkData = [
              'name'=>$approve_post_arr[0]->message,
              'link' =>$approve_post_arr[0]->url,
              'description'=>$approve_post_arr[0]->description,
              'message' => $approve_post_arr[0]->message,
              'url' => $approve_post_arr[0]->image,
              'caption' => $approve_post_arr[0]->caption
              ];

                    try {
                  $response = $FB->post(
                      '/me/photos',
                     $linkData,
                      $accessTokenLong
                    );

                } catch(Facebook\Exceptions\FacebookResponseException $e) {

                  $ErrorCode=$e->getCode();
                  if($ErrorCode==324){
                    $data_post = array(
                         'status' => 0
                      );
                    $this->db->where('id', $id);
                    $this->db->update('tbl_mymentor_posts', $data_post);

                    ms(array(
                   "st"    => "success",
                   "label" => "bg-red",
                   "txt"   => "<span class='col-white'>Missing or invalid image file</a></span>"
                 ));
                  }
                  exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                  echo 'Facebook SDK returned an error: ' . $e->getCode();
                  exit;
                }
                $user = $response->getGraphUser();

                $obj_id=$user['post_id'];
                //--------update tbl_mymentor_posts---------------------------------
                 $post_response_id = json_encode(array('object_id' =>$obj_id,'access_token'=>$accessTokenLong));
                 $data_post = array(
                      'post_response_id' => $post_response_id
                   );
                 $this->db->where('id', $id);
                 $this->db->update('tbl_mymentor_posts', $data_post);

                 //--------update tbl_mymentor_posts--------------------
                 //-----------start--------------Another Email for mentor notifications---------------------------------\
                $post_info_arr=get_SocailPostInformation_postID($id);
                $social_post_title=$post_info_arr[0]->post_title;
                $social_mentor_name=$this->input->post('txtMentornamePostApproval');
                $post_link=BASE."index.php/posts/view/".$id;
                $post_userid=$post_info_arr[0]->uid;
                $user_post_arr=getUserById($post_userid);
                $mentee_emailid=$user_post_arr[0]->email;
                date_default_timezone_set('UTC');
                $this->load->library('SendEmail', null, 'mailObj');
                $strpEmailTo =$mentee_emailid;
                $strpRecipientName = '';
                $arrpTags = array();
                $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
                $arrpTags['{--PostTitle--}'] = $social_post_title;
                $arrpTags['{--PostLink--}'] = $post_link;
                $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
                $arrpTags['{--SocialPlatform--}'] = 'Facebook';
                $arrpTags['{--SiteHomeURL--}'] = base_url();
                $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
                $arrpTags['{--LoginUrl--}'] = base_url();
                $strpTempleteid = 12;
                if($this->mailObj->sendEmail(
                   $strpEmailTo,
                   $strpRecipientName,
                   $arrpTags,
                   $strpTempleteid,
                   $AdminEmailId = '',
                   $AdminName = '')) ;
                //---------Stop-----------------------------------------------------------

                 ms(array(
                 "st"    => "success",
                 "label" => "bg-green",
                 "txt"   => "<span class='col-white'>Post Successfully posted </a></span>"
               ));



            }//end of photo post of facebook
            if($post_type=="images"){


                try {
                  $data = [
                    'message' => $approve_post_arr[0]->message,
                    'source' => $FB->fileToUpload($approve_post_arr[0]->image),
                  ];

                  //echo "6666A";
                  // Returns a `Facebook\FacebookResponse` object
                  $response = $FB->post('/me/photos', $data, $accessTokenLong);
                  //print_r($response);
                  //die;
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                  echo 'Graph returned an error: ' . $e->getMessage();
                  exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                  echo 'Facebook SDK returned an error: ' . $e->getMessage();
                  exit;
                }
                $user = $response->getGraphUser();
                $obj_id=$user['id'];
                //--------update tbl_mymentor_posts---------------------------------
                 $post_response_id = json_encode(array('object_id' =>$obj_id,'access_token'=>$accessTokenLong));
                 $data_post = array(
                      'post_response_id' => $post_response_id
                   );
                 $this->db->where('id', $id);
                 $this->db->update('tbl_mymentor_posts', $data_post);
                 //--------update tbl_mymentor_posts--------------------
                 //-----------start--------------Another Email for mentor notifications---------------------------------\
                $post_info_arr=get_SocailPostInformation_postID($id);
                $social_post_title=$post_info_arr[0]->post_title;
                $social_mentor_name=$this->input->post('txtMentornamePostApproval');
                $post_link=BASE."index.php/posts/view/".$id;
                $post_userid=$post_info_arr[0]->uid;
                $user_post_arr=getUserById($post_userid);
                $mentee_emailid=$user_post_arr[0]->email;
                date_default_timezone_set('UTC');
                $this->load->library('SendEmail', null, 'mailObj');
                $strpEmailTo =$mentee_emailid;
                $strpRecipientName = '';
                $arrpTags = array();
                $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
                $arrpTags['{--PostTitle--}'] = $social_post_title;
                $arrpTags['{--PostLink--}'] = $post_link;
                $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
                $arrpTags['{--SocialPlatform--}'] = 'Facebook';
                $arrpTags['{--SiteHomeURL--}'] = base_url();
                $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
                $arrpTags['{--LoginUrl--}'] = base_url();
                $strpTempleteid = 12;
                if($this->mailObj->sendEmail(
                   $strpEmailTo,
                   $strpRecipientName,
                   $arrpTags,
                   $strpTempleteid,
                   $AdminEmailId = '',
                   $AdminName = '')) ;
                //---------Stop-----------------------------------------------------------
                 ms(array(
                 "st"    => "success",
                 "label" => "bg-green",
                 "txt"   => "<span class='col-white'>Post Successfully posted </a></span>"
               ));


            }//end of photo post of facebook


            //for facebook video post_on_mentor
            if($post_type=="video"){
                $data = [
                'title' => $approve_post_arr[0]->caption,
                'description' => $approve_post_arr[0]->description,
                'source' => $FB->videoToUpload($approve_post_arr[0]->url),
              ];

                try {
                  $response = $FB->post('/me/videos', $data, $accessTokenLong);
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                  // When Graph returns an error
                  echo 'Graph returned an error: ' . $e->getMessage();
                  exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                  // When validation fails or other local issues
                  echo 'Facebook SDK returned an error: ' . $e->getMessage();
                  exit;
                }
                $user = $response->getGraphUser();
                $obj_id=$user['id'];
                //--------update tbl_mymentor_posts---------------------------------
                 $post_response_id = json_encode(array('object_id' =>$obj_id,'access_token'=>$accessTokenLong));
                 $data_post = array(
                      'post_response_id' => $post_response_id
                   );
                 $this->db->where('id', $id);
                 $this->db->update('tbl_mymentor_posts', $data_post);
                 //--------update tbl_mymentor_posts--------------------
                 //-----------start--------------Another Email for mentor notifications---------------------------------\
                $post_info_arr=get_SocailPostInformation_postID($id);
                $social_post_title=$post_info_arr[0]->post_title;
                $social_mentor_name=$this->input->post('txtMentornamePostApproval');
                $post_link=BASE."index.php/posts/view/".$id;
                $post_userid=$post_info_arr[0]->uid;
                $user_post_arr=getUserById($post_userid);
                $mentee_emailid=$user_post_arr[0]->email;
                date_default_timezone_set('UTC');
                $this->load->library('SendEmail', null, 'mailObj');
                $strpEmailTo =$mentee_emailid;
                $strpRecipientName = '';
                $arrpTags = array();
                $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
                $arrpTags['{--PostTitle--}'] = $social_post_title;
                $arrpTags['{--PostLink--}'] = $post_link;
                $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
                $arrpTags['{--SocialPlatform--}'] = 'Facebook';
                $arrpTags['{--SiteHomeURL--}'] = base_url();
                $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
                $arrpTags['{--LoginUrl--}'] = base_url();
                $strpTempleteid = 12;
                if($this->mailObj->sendEmail(
                   $strpEmailTo,
                   $strpRecipientName,
                   $arrpTags,
                   $strpTempleteid,
                   $AdminEmailId = '',
                   $AdminName = '')) ;
                //---------Stop-----------------------------------------------------------
                 ms(array(
                  "st"    => "success",
                  "label" => "bg-green",
                  "txt"   => "<span class='col-white'>Post Successfully posted </a></span>"
                ));

          }
            //-for facebook video for post


          }
          if($social_type=='twitter'){ //twitterpost



            $post_type=$approve_post_arr[0]->type;
            $user_id=$approve_post_arr[0]->uid;
            $this->db->select("*");
            $this->db->from(TWITTER_TB);
            $this->db->where('uid', $user_id);
            $query = $this->db->get();
            $result_array = $query->result_array();

            foreach ($result_array as $key => $value) {
            $name=$value['name'];
            $name=$value['screen_name'];
            $tid=$value['tid'];
            }
               $data = array(
               'type' => $post_type ,
               'post_title' => $approve_post_arr[0]->post_title,
               'fid' => $tid ,
               'account' => $user_id,
               'name'=>$name,
               'message'=>$approve_post_arr[0]->message,
               'time_post'=>date('Y-m-d h:i:s'),
               'url'=>$approve_post_arr[0]->url,
               'uid'=>$user_id,
               'title'=>$approve_post_arr[0]->title,
               'description'=>$approve_post_arr[0]->message,
               'status'=>0											);
                $this->db->insert('tbl_posts', $data);
                $post_id=$this->db->insert_id();
                $spintax = new Spintax();
                ini_set('max_execution_time', 300000);
                define("TIMEPOST",date("Y-m-d H:i").":00");
                $result = $this->db
                  ->select('*')
                  ->from(POSTS_TB)
                  ->where('status = ', 0)
                   ->where('account= ', $user_id)
                  ->get()->result();




                  if(!empty($result)){
                  foreach ($result as $key => $row) {

                     $delete       = $row->delete;
                     $repeat       = $row->repeat_post;
                     $repeat_time  = $row->repeat_time;
                     $repeat_end   = $row->repeat_end;
                     $time_post    = $row->time_post;
                     $deplay       = $row->deplay;

                     $time_post          = strtotime($time_post);
                     $time_post_only_day = date("Y-m-d", $time_post);
                     $time_post_day      = strtotime($time_post_only_day);
                     $repeat_end         = strtotime($repeat_end);

                    $row->type         = $spintax->process($post_type);
                    $row->url         = $spintax->process($approve_post_arr[0]->url);
                    $row->message     = $spintax->process($approve_post_arr[0]->message);
                    $row->title       = $spintax->process($approve_post_arr[0]->title);
                    $row->description = $spintax->process($approve_post_arr[0]->message);
                    $row->image       = $spintax->process($approve_post_arr[0]->image);
                    $row->caption     = $spintax->process($approve_post_arr[0]->caption);
                    $TW=$this->db->select('*')->from(TWITTER_TB)->where('uid = ', $row->account)->get()->result();

                    //$TW = $this->model->get("*", TWITTER_TB, "uid =58");





                if(!empty($TW)){
                  $row->access_token = $TW[0]->access_token;
                    //post now
                      $response = TW_POST($row);
                      $data = array(
                         'status' => 1
                      );
                      $this->db->where('account', $user_id);
                      $this->db->update(POSTS_TB, $data);

                      $data_w = array(
                         'post_response_id' => $response['id'],
                         'approved_by'=>$user_post_arr[0]->id
                      );
                      $this->db->where('id', $id);
                      $this->db->update('tbl_mymentor_posts', $data_w);
                      //-----------start--------------Another Email for mentor notifications---------------------------------\
                    $post_info_arr=get_SocailPostInformation_postID($id);
                    $social_post_title=$post_info_arr[0]->post_title;
                    $social_mentor_name=$this->input->post('txtMentornamePostApproval');
                    $post_link=BASE."index.php/posts/view/".$id;
                    $post_userid=$post_info_arr[0]->uid;
                    $user_post_arr=getUserById($post_userid);
                    $mentee_emailid=$user_post_arr[0]->email;
                    date_default_timezone_set('UTC');
                    $this->load->library('SendEmail', null, 'mailObj');
                    $strpEmailTo =$mentee_emailid;
                    $strpRecipientName = '';
                    $arrpTags = array();
                    $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
                    $arrpTags['{--PostTitle--}'] = $social_post_title;
                    $arrpTags['{--PostLink--}'] = $post_link;
                    $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
                    $arrpTags['{--SocialPlatform--}'] = 'Twitter';
                    $arrpTags['{--SiteHomeURL--}'] = base_url();
                    $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                    $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
                    $arrpTags['{--LoginUrl--}'] = base_url();
                    $strpTempleteid = 12;
                    if($this->mailObj->sendEmail(
                       $strpEmailTo,
                       $strpRecipientName,
                       $arrpTags,
                       $strpTempleteid,
                       $AdminEmailId = '',
                       $AdminName = '')) ;
                    //---------Stop-----------------------------------------------------------

                    $response['data']= '';
                    $response['message']= 'Post published now successfully';
                    $response['status']= 1;

                  $this->response($response, 200);


                }
              }
          }
          }//twitterpostend
          if($social_type=='linkedin-square'){
            $spintax = new Spintax();
             $post_type=$approve_post_arr[0]->type;
             $user_id=$approve_post_arr[0]->uid;
             $id=$approve_post_arr[0]->id;
             $result = $this->db
              ->select('*')
              ->from('tbl_mymentor_posts')
               ->where('id', $id)
              ->get()->result();

              if(!empty($result)){
                foreach ($result as $key => $row) {
                  $row->url         = $spintax->process($approve_post_arr[0]->url);
                 $row->message     = $spintax->process($approve_post_arr[0]->message);
                 $row->title       = $spintax->process($approve_post_arr[0]->title);
                 $row->description = $spintax->process($approve_post_arr[0]->description);
                 $row->image       = $spintax->process($approve_post_arr[0]->image);
                 $row->caption     = $spintax->process($approve_post_arr[0]->image);
                 $TW = $this->model->get("*", 'tbl_linkedin', "uid = '".$row->account."'");
                 if(!empty($TW)){
                    $row->access_token = $TW->access_token;
                     $response = LI_POST($row);
                     //-----------start--------------Another Email for mentor notifications---------------------------------\
                   $post_info_arr=get_SocailPostInformation_postID($id);
                   $social_post_title=$post_info_arr[0]->post_title;
                   $social_mentor_name=$this->input->post('txtMentornamePostApproval');
                   $post_link=BASE."index.php/posts/view/".$id;
                   $post_userid=$post_info_arr[0]->uid;
                   $user_post_arr=getUserById($post_userid);
                   $mentee_emailid=$user_post_arr[0]->email;
                   date_default_timezone_set('UTC');
                   $this->load->library('SendEmail', null, 'mailObj');
                   $strpEmailTo =$mentee_emailid;
                   $strpRecipientName = '';
                   $arrpTags = array();
                   $arrpTags['{--ApprovalMentorName--}'] = $social_mentor_name;
                   $arrpTags['{--PostTitle--}'] = $social_post_title;
                   $arrpTags['{--PostLink--}'] = $post_link;
                   $arrpTags['{--PostedOn--}'] = date('l  M j Y h:i:s A');
                   $arrpTags['{--SocialPlatform--}'] = 'Linkedin';
                   $arrpTags['{--SiteHomeURL--}'] = base_url();
                   $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                   $arrpTags['{--Subject--}'] = "IGGI | Post Approval Message";
                   $arrpTags['{--LoginUrl--}'] = base_url();
                   $strpTempleteid = 12;
                   if($this->mailObj->sendEmail(
                      $strpEmailTo,
                      $strpRecipientName,
                      $arrpTags,
                      $strpTempleteid,
                      $AdminEmailId = '',
                      $AdminName = '')) ;
                   //---------Stop-----------------------------------------------------------
                     ms(array(
                       "st"    => "success",
                       "label" => "bg-white",
                       "txt"   => "<span class='col-green'>Successfully Post</a></span>"
                     ));

                 }
                }

              }

          }
          if($social_type=='instagram'){
             $user_id=$approve_post_arr[0]->uid;
            //------------------------
            //------------A---------------------------
            $spintax = new Spintax();
             $data = array();
              $post_type=$approve_post_arr[0]->type;


             switch ($post_type) {
               case 'video':

                 $data = array(
                   "category"    => "post",
                   "type"        => $post_type,
                   "image"       => $spintax->process($approve_post_arr[0]->url),
                   "message"     => $spintax->process($approve_post_arr[0]->message),
                 );
                 break;
               default:


                 $data = array(
                   "category"  => "post",
                   "type"      =>$post_type,
                   "image"     => $spintax->process($approve_post_arr[0]->image),
                   "message"   => $spintax->process($approve_post_arr[0]->message)
                 );
                 break;
             }


             $inst_user_arr=getInstagramUser($user_id);
              $post_group=$inst_user_arr[0]->username;


             $account = $this->model->get("*", 'instagram_accounts', "username = '".$post_group."'");
            //  echo "string";
            //  print_r($account);
            //  die;

             if($post_group){
               $data["uid"]            = $user_id;
               $data["group_type"]     = "profile";
               $data["account_id"]     = $account->id;
               $data["account_name"]   = $post_group;
               $data["group_id"]       = $post_group;
               $data["name"]           = $post_group;
               $data["privacy"]        = 0;
               $data["time_post"]      = NOW;
               $data["changed"]        = NOW;
               $data["created"]        = NOW;
               $data["deplay"]         = 10;
               $data["status"]         = 1;

               $date = new DateTime(NOW, new DateTimeZone(TIMEZONE_SYSTEM));
               $date->setTimezone(new DateTimeZone(TIMEZONE_USER));
               $time_post_show = $date->format('Y-m-d H:i:s');

               $data["time_post_show"] = $time_post_show;
               if(!empty($account)){
                 $this->db->insert('instagram_schedules', $data);
                 $id_a = $this->db->insert_id();

                 $data['username'] = $account->username;
                 $data['password'] = $account->password;
                 $data['fid'] = $account->fid;
                 $row = (object)$data;


                 //---------------Save data on Successfully post--if auto post not active ----------------------------
                 $auto_post=0;
                 if(!$auto_post==0){
                   //save post to approved  then post
                   //tbl_mymentor_posts
                   $data = array(
                     'post_title'=>$txtPostTitle,
                    'social_media_type'=>'instagram',
                    'type' => $type_name,
                    'fid' => $account->fid,
                    'account' => $account->id,
                     'name'=>$post_group,
                    'access_token' =>'',
                    'cid' =>'',
                    'message' =>$message,
                    'title'=>$link_title,
                    'description'=>$link_description,
                    'url'=>$link_url,
                    'image' =>$image_url,
                    'caption' =>$link_caption,
                    'time_post' =>date('Y-m-d h:i:s'),
                    'uid' =>$user_post_arr[0]->id,
                    'status' => 0,
                    'created'=>date('Y-m-d h:i:s')
                 );

                 $id=$this->db->insert('tbl_mymentor_posts', $data);
                 $response['status']='success';
                 $msg="Your post is successfully sent for approval";

                   //post now

                 }else{

                   $response = (object)Instagram_Post($row);



                   //--------update tbl_mymentor_posts
                    $post_response_id = json_encode(array('id' =>$response->id,'code'=>$response->code ));
                   $data_post = array(
                         'post_response_id' => $post_response_id
                      );

                    $this->db->where('id', $id);
                    $this->db->update('tbl_mymentor_posts', $data_post);

                   //--------------------------------

                    if($response->st == "success"){
                     ms(array(
                       "st"    => "success",
                       "label" => "bg-green",
                       "txt"   => "<span class='col-white'>".$msg." </a></span>"
                     ));
                   }else{
                     ms(array(
                       "st"    => "error",
                       "label" => "bg-red",
                       "txt"   => "<span class='col-red'>".$response->txt." </a></span>"
                     ));
                   }

               }//end if else
                 //------------------------------------------------------------------

                 if($response['status'] == "success"){
                   ms(array(
                     "st"    => "success",
                     "label" => "bg-light-green",
                     "txt"   => "<span class='col-green'>".$msg." </a></span>"
                   ));
                 }else{
                   ms(array(
                     "st"    => "error",
                     "label" => "bg-red",
                     "txt"   => "<span class='col-red'>".$response->txt."</span>"
                   ));
                 }
               }else{
                 ms(array(
                   "st"    => "error",
                   "label" => "bg-red",
                   "txt"   => "<span class='col-red'>".l('Instagram account not exist')."</span>"
                 ));
               }
             }


            //---------instagram end---------------

          }//end of instagram






          $query = $this->db->query($SQL);

          //------send email notifications to mentee that your

          //------------------------------------------------------

          }
          function mentorRegister_post(){
             $email      = $this->post('email');
             $password     = $this->post('password');
             $device_id     = $this->post('device_id');
             $device_type     = $this->post('device_type');
             $device_name     = $this->post('device_name');
             $query = $this->db->get_where('user_management',array('email'=>$email));
              if($query->num_rows() > 0){
                $response['data']= (object)array();
                $response['message']= 'This email is already associated with IGGI APP';
                $response['status']= 0;
              }else{
                $data = array(
        					  'fullname'=> '',
        					  'type'    => 'mobileAPP',
        		    		'email'   => $email,
        		    		'password'=> md5($password),
        		    		'changed' => Date('Y-m-d H:i:s'),
        		    		'created' => Date('Y-m-d H:i:s'),
        						'user_type'=>'0',
        						'post_right'=>'0'
        		    	);
                 $id = $this->Iggi_model->RegisterMentor($data);
          		   $insert_id = $this->db->insert_id();

                 //get data
                 $char = "bcdfghjkmnpqrstvzBCDFGHJKLMNPQRSTVWXZaeiouyAEIOUY";
                 $token = '';
                 for ($i = 0; $i < 40; $i++) $token .= $char[(rand() % strlen($char))];

                     if($device_type==0){
                       $data_token = array(
                            'user_access_token' => $token."18IGGI$".$insert_id,
                            'iphone_devive_id'=>$device_id,
                            'ios_device_name'=>$device_name
                       );
                     }else{
                       $data_token = array(
                            'user_access_token' => $token."18IGGI$".$insert_id,
                            'android_devive_id'=>$device_id,
                            'android_device_name'=>$device_name
                       );
                     }

                 $this->db->where('id', $insert_id);
                 $this->db->update('user_management', $data_token);

                 $query = $this->db->get_where('user_management',array('email'=>$email,'password'=>md5($password)));
                 $result_arr_new=$query->result();
                 $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
                 $pro_phto=$result_arr_new[0]->profile_pic;
                 if(empty($pro_phto)){
                    $pro_img_url=BASE."uploads/profile/avator-default.png";
                 }else{
                   if($pro_pic_arr->pic_from=='self'){
                     $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
                   }else{
                     $pro_img_url= $result_arr_new[0]->profile_pic;
                   }
                 }
                 if(empty($result_arr_new[0]->iphone_devive_id)){
                   $ios_device_status=0;
                 }else{
                   $ios_device_status=1;
                 }
                 if(empty($result_arr_new[0]->android_devive_id)){
                   $android_devive_id_status=0;
                 }else{
                   $android_devive_id_status=1;
                 }
                 $user_arr=array(
                   'email'=>$result_arr_new[0]->email,
                   'status'=>$result_arr_new[0]->status,
                   'user_type'=>intval($result_arr_new[0]->user_type),
                   'post_right'=>intval($result_arr_new[0]->post_right),
                   'first_name'=>is_null($result_arr_new[0]->first_name) ? '':$result_arr_new[0]->first_name,
                   'last_name'=>$result_arr_new[0]->last_name,
                   'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
                   'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
                   'profile_pic'=>$pro_img_url,
                   'mentor_type_other'=>is_null($result_arr_new[0]->mentor_type_other) ? '':$result_arr_new[0]->mentor_type_other,
                   'user_access_token'=>is_null($result_arr_new[0]->user_access_token) ? '':$result_arr_new[0]->user_access_token,
                   'ios_device_status'=>$ios_device_status,
                   'android_devive_id_status'=>$android_devive_id_status,
                   'profile_complete_check'=>$result_arr_new[0]->first_name=="" ? 0:1

                 );


                 $arrLoginResponse =array(
                   'userinfo' =>$user_arr,
                   'subscription'=>(object)array()
                 );

                 $user_arr['userid']=$insert_id;
          			$response['data']= $arrLoginResponse;
                $response['message']= 'Sign Up success';
                $response['status']= 1;

              }

           $this->response($response, 200);

          }

          //profile update
          function updateUserProfile_post(){
            $header_data=$this->input->request_headers();
            $token_withuserid=$header_data['iggi_token'];
            $token_arr = explode("18IGGI$", $token_withuserid);

             $user_id    = $token_arr[1];
             $first_name    = $this->post('first_name');
             $last_name     = $this->post('last_name');
             $usertype     = $this->post('usertype');
             $mentor_type     = $this->post('mentor_type');
             $org_type     = $this->post('org_type');

             if($user_id==""){
               $response['data']=array();
               $response['message']= "userid is required";
               $response['status']= 0;
               $this->response($response, 200);
             }
             if($first_name==""){
               $response['data']=array();
               $response['message']= "first_name is required";
               $response['status']= 0;
               $this->response($response, 200);
             }
             if($last_name==""){
               $response['data']=array();
               $response['message']= "last_name is required";
               $response['status']= 0;
               $this->response($response, 200);
             }


             if($usertype==1){
               //mentee updatetion
               $data_update = array(
                          'first_name' => $first_name,
                          'last_name' =>$last_name,
                          'org_type'=>$org_type,
                          'mentor_type'=>$mentor_type
              );
           		$this->db->where('id', $user_id);
           		$this->db->update('user_management', $data_update);
              $response['data']= '';
              $response['message']= 'Profile Updated';
              $response['status']= 1;
             }else{
               //Mentor updatetion
               $data_update = array(
                          'first_name' => $first_name,
                          'last_name' =>$last_name,
                          'org_type'=>$org_type,
                          'mentor_type'=>$mentor_type
              );
           		$this->db->where('id', $user_id);
           		$this->db->update('user_management', $data_update);
              $response['data']= array();
              $response['message']= 'Profile Updated';
              $response['status']= 1;
             }

              $this->response($response, 200);
          }

          //get mentor type
          function getMentorType_get(){

              $query = $this->db->get('tbl_mentor_type');
               if($query->num_rows() > 0){
                 $arr=$query->result();
                 $array_data = array();
                 $z=array();


                 foreach ($arr as $key => $value) {

                   array_push($z,is_null($value->mentor_name) ? '':$value->mentor_name);
                    }
                  $response['data']=$z;
                  $response['message']= " List of mentor type";
                  $response['status']= 1;

              }else{
                $response['data']=array();
                $response['message']= " No mentor type found";
                $response['status']= 0;
              }

                 $this->response($response, 200);

          }
          //get raltion for mentee
          //get mentor type
          function getMenteeType_get(){

              $query = $this->db->get('tbl_relation_role');
               if($query->num_rows() > 0){
                 $arr=$query->result();
                 $array_data = array();
                 $z=array();


                 foreach ($arr as $key => $value) {

                   array_push($z,is_null($value->relation_role_name) ? '':$value->relation_role_name);
                    }
                  $response['data']=$z;
                  $response['message']= " List of mentee type";
                  $response['status']= 1;

              }else{
                $response['data']=array();
                $response['message']= " No mentee type found";
                $response['status']= 0;
              }

                 $this->response($response, 200);

          }


          function getForgetPassword_post(){
             $email    = $this->post('email');
             $query = $this->db->get_where('user_management',array('email'=>$email));
              if($query->num_rows() > 0){
                function randomPassword() {
                $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
                $pass = array(); //remember to declare $pass as an array
                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                for ($i = 0; $i < 8; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];                }
                return implode($pass); //turn the array into a string
            }
                      //----------------send forget password
                      $txtForgetEmail=$email;
                      $this->load->library('SendEmail', null, 'mailObj');
                      $strpEmailTo = $txtForgetEmail;
                      $strpRecipientName = '';
                      $arrpTags = array();
                      $txtPass=randomPassword();

                      $data = array(
                           'password'=> md5($txtPass),

                        );

                      $this->db->where('email', $txtForgetEmail);
                      $this->db->update('user_management', $data);
                      $arrpTags['{--SiteHomeURL--}'] = base_url();
                      $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
                      $arrpTags['{--EmailId--}'] = $strpEmailTo;
                      $arrpTags['{--Subject--}'] = "Forget Password | IGGI ";
                      $arrpTags['{--MyAuthPassword--}'] = $txtPass;

                      $strpTempleteid = 3;

                      if($this->mailObj->sendEmail(
                         $strpEmailTo,
                         $strpRecipientName,
                         $arrpTags,
                         $strpTempleteid,
                         $AdminEmailId = '',
                         $AdminName = '')) ;



                $response['data']= $email;
                $response['message']= 'Credentials sent to your email.';
                $response['status']= 1;

              }else{
                $result_arr=$query->result();
                $response['data']= array();
                $response['message']= 'Invalid Credentials ';
                $response['status']= 0;
              }
              $this->response($response, 200);


          }
          //mentor request
          function sendRequestMentor_post(){
            $txtMentorInviteEmail=$this->input->post('mentorInviteEmail');
            if(empty($txtMentorInviteEmail)){
              $response['data']=array();
              $response['message']= "Email is required";
              $response['status']= 0;
              $this->response($response, 200);
            }

            if (!filter_var($txtMentorInviteEmail, FILTER_VALIDATE_EMAIL)) {
              $response['data']=array();
              $response['message']= "Invalid Email";
              $response['status']= 0;
              $this->response($response, 200);
            }



              $this->load->library('SendEmail', null, 'mailObj');
              $strpEmailTo = $txtMentorInviteEmail;
              $strpRecipientName = '';
              $arrpTags = array();
              $arrpTags['{--SiteHomeURL--}'] = base_url();
              $arrpTags['{--SiteHomeURLlogo--}'] = base_url()."theme/images/logo.png";
              $arrpTags['{--Subject--}'] = "IGGI Invitation";
              $arrpTags['{--LoginUrl--}'] = base_url();
              $strpTempleteid = 6;
              if($this->mailObj->sendEmail(
                 $strpEmailTo,
                 $strpRecipientName,
                 $arrpTags,
                 $strpTempleteid,
                 $AdminEmailId = '',
                 $AdminName = '')) ;
                 $response['data']=array();
                 $response['message']= "sent request successfully";
                 $response['status']= 1;
                 $this->response($response, 200);
          }
          //this api for social login for mentor facebook
          function setSocialMentor_post(){
             $login_type=$this->input->post('login_type'); //login socal name
             $facebook_id=$this->input->post('login_id'); //id of social media
             $facebook_email=$this->input->post('login_email'); //if email is provide the it ok other wise need email
             $facebook__fullname=$this->input->post('login__fullname');
             $first_name=$this->input->post('first_name');
             $last_name=$this->input->post('last_name');
             $facebook__profile_pic=$this->input->post('login__profile_pic');
             $data = array(
              "type"            => $login_type,
              "pid"             => $facebook_id,
              "fullname"        => $facebook__fullname,
              "status"          => '1',
              "timezone"				=>'Asia/Kolkata',
              'first_name'			=>$first_name,
              'last_name'			=>$last_name,
              'profile_pic'		=>$facebook__profile_pic,
            );
              if(isset($facebook_email)){
                $data['email'] = $facebook_email;
              }

              if(isset($data['email'])){
                $result = $this->db->get_where('user_management',array('email'=>$facebook_email));
      			}else{
               $result = $this->db->get_where('user_management',array('pid'=>$facebook_id));
      			}
             $result_arr=$result->result();
             $result_arr = array_filter($result_arr);
             if (!empty($result_arr)){
                $this->db->update('user_management', $data, array('id' => $result_arr[0]->id));

                //response login details --
                $query = $this->db->get_where('user_management',array('id'=>$result_arr[0]->id));
                $result_arr_new=$query->result();
                $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
                $pro_phto=$result_arr_new[0]->profile_pic;
                if(empty($pro_phto)){
                   $pro_img_url=BASE."uploads/profile/avator-default.png";
                }else{
                  if($pro_pic_arr->pic_from=='self'){
                    $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
                  }else{
                    $pro_img_url= $result_arr_new[0]->profile_pic;
                  }
                }
                if(empty($result_arr_new[0]->iphone_devive_id)){
                  $ios_device_status=0;
                }else{
                  $ios_device_status=1;
                }
                if(empty($result_arr_new[0]->android_devive_id)){
                  $android_devive_id_status=0;
                }else{
                  $android_devive_id_status=1;
                }



                $user_arr=array(
                  'email'=>$result_arr_new[0]->email,
                  'status'=>$result_arr_new[0]->status,
                  'user_type'=>intval($result_arr_new[0]->user_type),
                  'post_right'=>intval($result_arr_new[0]->post_right),
                  'first_name'=>is_null($result_arr_new[0]->first_name) ? '':$result_arr_new[0]->first_name,
                  'last_name'=>is_null($result_arr_new[0]->last_name) ? '':$result_arr_new[0]->last_name,
                  'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
                  'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
                  'profile_pic'=>$pro_img_url,
                  'mentor_type_other'=>is_null($result_arr_new[0]->mentor_type_other) ? '':$result_arr_new[0]->mentor_type_other,
                  'user_access_token'=>is_null($result_arr_new[0]->user_access_token) ? '':$result_arr_new[0]->user_access_token,
                  'ios_device_status'=>$ios_device_status,
                  'android_devive_id_status'=>$android_devive_id_status,
                  'profile_complete_check'=>$result_arr_new[0]->first_name=="" ? 0:1

                );


                $arrLoginResponse =array(
                  'userinfo' =>$user_arr,
                  'subscription'=>(object)array()
                );

                //response login details ---

                $response['data']=$arrLoginResponse;
                $response['message']= "Successfully Logged In";
                $response['status']= 1;
              }
            else
              {
                 $this->db->insert('user_management', $data);
                 $id = $this->db->insert_id();
                 $use_id=$id;
                 $result = $this->db->get_where('user_management',array('id'=>$id));
                  $plan=75;//this is basic plan id need to update with function
                  $subs_arr=get_subscriptions_byid($plan);
                  $subs_interval=$subs_arr[0]->subs_interval;
                  switch ($subs_interval) {
                    case 'lifetime':
                      $exp_days="+10 years";
                      $date = strtotime(date('Y-m-d H:i:s'));
                      $date_exp = strtotime($exp_days, $date);
                      $futue_date= date('Y-m-d H:i:s', $date_exp);
                      break;
                    default:
                      # code...
                      break;
                  }
                  $defaut_plan_arr = array(
                  'plan_type' =>'general',
                  'plan_name' =>$subs_arr[0]->plan_name,
                  'user_id' =>$id,
                  'plan_id' =>$subs_arr[0]->id,
                  'plan_details' =>'',
                  'no_mentee'=>$subs_arr[0]->no_mentee,
                  'no_mentor'=>$subs_arr[0]->no_mentor,
                  'no_platform'=>$subs_arr[0]->no_platform,
                  'no_device'=>$subs_arr[0]->no_platform,
                  'created_on'=>date('Y-m-d H:i:s'),
                  'updated_on'=>date('Y-m-d H:i:s'),
                  'expired_on'=>$futue_date,
                  'status'=>'3',
                  'qty'=>'1',
                  'plan_interval'=>$subs_interval
                );
                $this->db->insert('tbl_membership', $defaut_plan_arr);

                //response login details --
                $query = $this->db->get_where('user_management',array('id'=>$use_id));
                $result_arr_new=$query->result();
                $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
                $pro_phto=$result_arr_new[0]->profile_pic;
                if(empty($pro_phto)){
                   $pro_img_url=BASE."uploads/profile/avator-default.png";
                }else{
                  if($pro_pic_arr->pic_from=='self'){
                    $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
                  }else{
                    $pro_img_url= $result_arr_new[0]->profile_pic;
                  }
                }
                if(empty($result_arr_new[0]->iphone_devive_id)){
                  $ios_device_status=0;
                }else{
                  $ios_device_status=1;
                }
                if(empty($result_arr_new[0]->android_devive_id)){
                  $android_devive_id_status=0;
                }else{
                  $android_devive_id_status=1;
                }
                $user_arr=array(
                  'email'=>$result_arr_new[0]->email,
                  'status'=>$result_arr_new[0]->status,
                  'user_type'=>intval($result_arr_new[0]->user_type),
                  'post_right'=>intval($result_arr_new[0]->post_right),
                  'first_name'=>is_null($result_arr_new[0]->first_name) ? '':$result_arr_new[0]->first_name,
                  'last_name'=>$result_arr_new[0]->last_name,
                  'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
                  'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
                  'profile_pic'=>$pro_img_url,
                  'mentor_type_other'=>is_null($result_arr_new[0]->mentor_type_other) ? '':$result_arr_new[0]->mentor_type_other,
                  'user_access_token'=>is_null($result_arr_new[0]->user_access_token) ? '':$result_arr_new[0]->user_access_token,
                  'ios_device_status'=>$ios_device_status,
                  'android_devive_id_status'=>$android_devive_id_status,
                  'profile_complete_check'=>$result_arr_new[0]->first_name=="" ? 0:1

                );


                $arrLoginResponse =array(
                  'userinfo' =>$user_arr,
                  'subscription'=>(object)array()
                );

                //response login details ---

                $response['data']=$arrLoginResponse;
                $response['message']= "Successfully , signup with ".$login_type;
                $response['status']= 1;

              }
               $this->response($response, 200);

          }
          //this is used to get input  passcode and validated true and false
          function menteeRegisterCodeInput_post(){
                 $txtInvitationCode=$this->input->post('passcode');
                 $query = $this->db->get_where('tbl_mentor_mentee',array('activelinkkey'=>$txtInvitationCode,'status'=>'0'));
                 $mentee_arr=$query->result();

                 $added_by=getUserById($mentee_arr[0]->added_by);

                 $arr_mentee_data= array(
                   'user_type'=>$mentee_arr[0]->user_type,
                   'email'=>$mentee_arr[0]->email,
                   'mentor_name'=>$added_by[0]->first_name." ".$added_by[0]->last_name
                 );

                   if($query->num_rows() > 0){
                     $response['data']= $arr_mentee_data;
                     $response['message']= 'verified';
                     $response['status']= 1;
                   }else{
                     $response['data']= (object)array();
                     $response['message']= 'unverified';
                     $response['status']= 0;
                   }
                   $this->response($response, 200);
            }
            //this is used to register mentee
            function menteeRegister_post(){
                 $email=$this->input->post('email');
                 $firstname=$this->input->post('firstname');
                 $lastname=$this->input->post('lastname');
                 $mentor_email=$this->input->post('mentor_email');
                 $mentor_name=$this->input->post('mentor_name');
                 $password=$this->input->post('password');
                 $invitationId=$this->input->post('invitationId');
                 $menteeRelationName=$this->input->post('menteeRelationName');

                 $query = $this->db->get_where('user_management',array('email'=>$email));
                  if($query->num_rows() > 0){

                    $response['data']= array();
                    $response['message']= $email." is already associated with iGGi.";
                    $response['status']= 0;
                  }else{
                    $data = array(
          					  'type'    => 'direct',
          		    		'email'   => $email,
          		    		'password'=> md5($password),
          		    		'status'  => 1,
          		    		'changed' => date('Y-m-d H:i:s'),
          		    		'created' => date('Y-m-d H:i:s'),
          						'first_name' => $firstname,
          						'last_name' => $lastname,
          						'user_type'=>'1',
          						'mentor_type'=>$menteeRelationName,
          						'post_right'=>'1'
          		    	);
                    $this->db->insert('user_management', $data);
          	    		$insert_id = $this->db->insert_id();
                    $ida= $insert_id;
                    $folder_user=FCPATH."uploads/user".$ida;
                    mkdir($folder_user);
                    $data_me = array(
                         'status' => '1',
                         'uid'=>$ida
                      );
                    $this->db->where('id',$invitationId);
                    $this->db->update('tbl_mentor_mentee', $data_me);

                    $this->load->library('SendEmail', null, 'mailObj');
                      $strpEmailTo =$email;
                      $strpRecipientName = '';
                      $arrpTags = array();
                      $arrpTags['{--RecipientName--}'] = $strpRecipientName;
                      $arrpTags['{--SiteHomeURL--}'] = base_url();
                      $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";
                      $arrpTags['{--EmailId--}'] =$email;
                      $arrpTags['{--MyAuthPassword--}'] = $password;
                      $arrpTags['{--Subject--}'] = "IGGI | Account Login Details";
                      $arrpTags['{--LoginUrl--}'] = base_url();
                     // $arrpTags['{--InvitedBy--}'] = '<a href="#" title="'.$usr_arr[0]->email.'">"'.$usr_arr[0]->first_name.'"</a>';

                      $strpTempleteid = 5;
                      if($this->mailObj->sendEmail(
                         $strpEmailTo,
                         $strpRecipientName,
                         $arrpTags,
                         $strpTempleteid,
                         $AdminEmailId = '',
                         $AdminName = '')) ;
                      //-------------------------Another Email for mentor notifications---------------------------------\
                      $this->load->library('SendEmail', null, 'mailObj');
                      $strpEmailTo =$mentor_email;
                      $strpRecipientName = '';
                      $arrpTags = array();
                      $arrpTags['{--RecipientName--}'] = $mentor_name;
                      $arrpTags['{--SiteHomeURL--}'] = base_url();
                      $arrpTags['{--SiteHomeURLlogo--}'] = "http://mymentor.checkdots.com/theme/images/logo.png";

                      $arrpTags['{--Subject--}'] = "IGGI | Mentee Register notifications";
                      $arrpTags['{--LoginUrl--}'] = base_url();
                      $strpTempleteid = 11;
                      if($this->mailObj->sendEmail(
                         $strpEmailTo,
                         $strpRecipientName,
                         $arrpTags,
                         $strpTempleteid,
                         $AdminEmailId = '',
                         $AdminName = '')) ;

                    //after signup metee get login details
                    $query = $this->db->get_where('user_management',array('email'=>$email,'password'=>md5($password)));
                    $result_arr_new=$query->result();
                    $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
                    $pro_phto=$result_arr_new[0]->profile_pic;
                    if(empty($pro_phto)){
                       $pro_img_url=BASE."uploads/profile/avator-default.png";
                    }else{
                      if($pro_pic_arr->pic_from=='self'){
                        $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
                      }else{
                        $pro_img_url= $result_arr_new[0]->profile_pic;
                      }
                    }
                    if(empty($result_arr_new[0]->iphone_devive_id)){
                      $ios_device_status=0;
                    }else{
                      $ios_device_status=1;
                    }
                    if(empty($result_arr_new[0]->android_devive_id)){
                      $android_devive_id_status=0;
                    }else{
                      $android_devive_id_status=1;
                    }
                    $user_arr=array(
                      'email'=>$result_arr_new[0]->email,
                      'status'=>$result_arr_new[0]->status,
                      'user_type'=>intval($result_arr_new[0]->user_type),
                      'post_right'=>intval($result_arr_new[0]->post_right),
                      'first_name'=>is_null($result_arr_new[0]->first_name) ? '':$result_arr_new[0]->first_name,
                      'last_name'=>$result_arr_new[0]->last_name,
                      'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
                      'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
                      'profile_pic'=>$pro_img_url,
                      'mentor_type_other'=>is_null($result_arr_new[0]->mentor_type_other) ? '':$result_arr_new[0]->mentor_type_other,
                      'user_access_token'=>is_null($result_arr_new[0]->user_access_token) ? '':$result_arr_new[0]->user_access_token,
                      'ios_device_status'=>$ios_device_status,
                      'android_devive_id_status'=>$android_devive_id_status,
                      'profile_complete_check'=>$result_arr_new[0]->first_name=="" ? 0:1

                    );
                    $arrLoginResponse =array(
                      'userinfo' =>$user_arr
                    );

                    $response['data']= $arrLoginResponse;
                    $response['message']= 'Mentee added successfully. ';
                    $response['status']= 1;
                  }
                  $this->response($response, 200);

            }
            //this api is used to show list of mentee and social media status as per mentor_id
            function getmenteeListwithSocialStatus_post(){
             $mentor_id=$this->input->post('mentor_id');
             $user_mentor_arr=getUserById($mentor_id);
             $user_mentor_name=$user_mentor_arr[0]->first_name." ".$user_mentor_arr[0]->last_name;
             $pro_pic_arr=json_decode($user_mentor_arr[0]->profile_pic);
             $pro_phto=$user_mentor_arr[0]->profile_pic;
             if(empty($pro_phto)){
                $pro_img_url=BASE."uploads/profile/avator-default.png";
             }else{
               if($pro_pic_arr->pic_from=='self'){
                 $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
               }else{
                 $pro_img_url= $result_arr_new[0]->profile_pic;
               }
             }

             $mentor_info_arr['mentor_info'] = array(
               'mentor_name' => is_null($user_mentor_namee) ? '':$user_mentor_name,
               'mentor_id' => $mentor_id,
               'profile_url'=>$pro_img_url
             );

             $data_arr_show=array();
              if($mentor_id==""){
                $response['data']=array();
                $response['message']= "mentor_id is required";
                $response['status']= 0;
                $this->response($response, 200);
              }else{
                   $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>'1','added_by'=>$mentor_id));
                   if($query->num_rows() > 0){
                     $data_arr=$query->result();

                     foreach ($data_arr as $key => $value) {
                        $mentee_id=$value->uid;
                        $user_arr=getUserById($mentee_id);
                        $user_name=$user_arr[0]->first_name." ".$user_arr[0]->last_name;
                        $user_info_arr = array(
                          'user_name' => $user_name,
                          'profile_url'=>$pro_img_url

                        );

                        //-------------------
                        $gooleplus_arr=getGooglePLUSUser($mentee_id);
                        $gooleplus_arr = array_filter($gooleplus_arr);
                        $gplus=0;
                        if (!empty($gooleplus_arr)) {
                          $gplus=1;
                        }
                        $instagram_arr=getInstagramUser($mentee_id);
                        $instagram_arr = array_filter($instagram_arr);
                        $inst=0;
                        if (!empty($instagram_arr)) {
                          $inst=1;
                        }


                        $linkedin_arr=getLinkedinUser($mentee_id);
                        $linkedin_arr = array_filter($linkedin_arr);
                        $li=0;
                        if (!empty($linkedin_arr)) {
                            $li=1;
                        }

                        $facebook_arr=getFacebookUser($mentee_id);
                        $facebook_arr = array_filter($facebook_arr);
                        $fb=0;
                        if (!empty($facebook_arr)) {
                          $fb=1;
                        }
                          $twitter_arr=getTwitterUser($mentee_id);
                          $twitter_arr = array_filter($twitter_arr);
                          $tw=0;
                          if (!empty($twitter_arr)) {
                            $tw=1;
                          }
                          $arrayFbSocial = array(
                            'facebook' =>$fb ,
                            'twitter' =>$tw ,
                            'linkedin'=>$li,
                            'instagram'=>$inst,
                            'google_plus'=>$gplus,

                          );
                        //-------------------

                        $data_arr_show['mentee_info'][]=array_merge($arrayFbSocial,(array)$value,$user_info_arr);



                     }

                     $response['data']=array_merge($mentor_info_arr,$data_arr_show);
                     $response['message']= "list of mentee with social status";
                     $response['status']= 1;
                     $this->response($response, 200);


                   }else{
                     $mentee_info['mentee_info']=array(

                     );
                     $response['data']=array_merge($mentor_info_arr,$mentee_info);
                     $response['message']= "no mentee is added yet";
                     $response['status']= 0;
                     $this->response($response, 200);
                   }
              }
            }
            //this api is used to nofification
            function setUserNotification_post(){
              $header_data=$this->input->request_headers();
              $token_withuserid=$header_data['iggi_token'];
              $token_arr = explode("18IGGI$", $token_withuserid);
              $user_id      = $token_arr[1];



              $notify_name=$this->input->post('notify_name');
        			$notify_val=$this->input->post('notify_val');
        			switch ($notify_name) {
        				case 'notify_inbox':
        				$data = array(
                       'inbox_email_notify' => $notify_val
                    );
        				$this->db->where('id', $user_id);
        				$this->db->update('user_management', $data);
                $response['data']=array();
                $response['message']= "Inbox Notifications Updated";
                $response['status']= 1;
                $this->response($response, 200);

        					break;
        				case 'post_content_email_notify':
                  $data = array(
                         'post_content_email_notify' => $notify_val
                      );
          				$this->db->where('id', $user_id);
          				$this->db->update('user_management', $data);
                  $response['data']=array();
                  $response['message']= "Post Email Notifications Updated";
                  $response['status']= 1;
                  $this->response($response, 200);

        					break;
        				  case 'mentee_post_approval_notify':
                  $data = array(
                         'mentee_post_approval_notify' => $notify_val
                      );
          				$this->db->where('id', $user_id);
          				$this->db->update('user_management', $data);
                  $response['data']=array();
                  $response['message']= "Post Email Notifications Updated";
                  $response['status']= 1;
                  $this->response($response, 200);
        					break;

                  case 'mentee_post_newcontent_notify':
                    $data = array(
                           'mentee_post_newcontent_notify' => $notify_val
                        );
            				$this->db->where('id', $user_id);
            				$this->db->update('user_management', $data);
                    $response['data']=array();
                    $response['message']= "Post Email Notifications Updated";
                    $response['status']= 1;
                    $this->response($response, 200);
          					break;

        				default:
        					// code...
        					break;
        			}


            }
            //this api is used to get status of notificaton with value --
            function getUserNotification_post(){
              $header_data=$this->input->request_headers();
              $token_withuserid=$header_data['iggi_token'];
              $token_arr = explode("18IGGI$", $token_withuserid);
              $user_id      = $token_arr[1];
              $query = $this->db->get_where('user_management',array('id'=>$user_id));
              $arr_data = array();
              if($query->num_rows() > 0){
                 $data_arr=$query->result();

                 $arr_data['inbox_email_notify']=intval($data_arr[0]->inbox_email_notify);
                 $arr_data['post_content_email_notify']=intval($data_arr[0]->post_content_email_notify);
                 $arr_data['mentee_post_approval_notify']=intval($data_arr[0]->mentee_post_approval_notify);
                 $arr_data['mentee_post_newcontent_notify']=intval($data_arr[0]->mentee_post_newcontent_notify);
                 $response['data']=$arr_data;
                 $response['message']= "Invalid Authorization";
                 $response['status']= 1;
                 $this->response($response, 200);


              }else{
                $response['data']=$arr_data;
                $response['message']= "Invalid Authorization";
                $response['status']= 1;
                $this->response($response, 200);
              }

            }
            //this api is used to get status of notificaton with value ---
            private	function addon_mailchip($email,$first_name,$last_name,$list_id,$subscription_type){
          		$result = $this->mailchimp->post("lists/$list_id/members", [
          										'email_address' => $email,
          										'merge_fields' => ['FNAME'=>$first_name, 'LNAME'=>$last_name,'SUBSCRIPTI'=>$subscription_type],
          										'status'        => 'subscribed',
          		 ]);
          		return "Subscribed successfully ".$result['merge_fields']['SUBSCRIPTI'];

          	}
          	private	function updateon_mailchip($email,$list_id,$subscription_type){
          		$userid_email = md5($email);
          		$result = $this->mailchimp->patch("lists/$list_id/members/$userid_email", [
          										'merge_fields' => ['SUBSCRIPTI'=>$subscription_type],
          										'status'        => 'subscribed',
          		 ]);
          		 return "Subscribed successfully ".$result['merge_fields']['SUBSCRIPTI'];

          	}
          	private	function unsubscribe_mailchip($email,$list_id){
          		$userid_email = md5($email);
          		$result = $this->mailchimp->patch("lists/$list_id/members/$userid_email", [
          										'status'        => 'unsubscribed',
          		 ]);
          		 return "Unsubscribed successfully ".$result['merge_fields']['SUBSCRIPTI'];

          	}

            //this api  is used to setMaichip
            function getSetEmailSubscription_post(){
              $header_data=$this->input->request_headers();
              $token_withuserid=$header_data['iggi_token'];
              $token_arr = explode("18IGGI$", $token_withuserid);
              $user_id      = $token_arr[1];
              //------------------------------------------
              $list_id='541a3a8d78';
            	$email_subsription=$this->input->post('subscription_type');
              if(empty($user_id)){
                $response['data']=array();
                $response['message']= "Missing token";
                $response['status']= 0;
                $this->response($response, 200);
              }

              if(empty($email_subsription)){
                $response['data']=array();
                $response['message']= "Subscription  Missing";
                $response['status']= 0;
                $this->response($response, 200);
              }
            	$usr_arr=getUserById($user_id);
            	$email=$usr_arr[0]->email;
            	$first_name=$usr_arr[0]->first_name;
            	$last_name=$usr_arr[0]->last_name;
            	if(empty($email)){
                $response['data']=array();
                $response['message']= "Update Email on profile setting";
                $response['status']= 0;
                $this->response($response, 200);

            	}
            	if(empty($first_name)){
                $response['data']=array();
                $response['message']= "Update First Name on profile setting";
                $response['status']= 0;
                $this->response($response, 200);

            	}
            	if(empty($last_name)){
                $response['data']=array();
                $response['message']= "Update Last Name on profile setting";
                $response['status']= 0;
                $this->response($response, 200);

            	}
            	//-------------------------
            	switch ($email_subsription) {
            		case 'daily':
            				$query = $this->db->get_where('tbl_emailsubcriber',array('email'=>$email));
            				if($query->num_rows() > 0){
            					$data = array(
            					 'subscribe_type' => 'daily'
            					);
            					$this->db->where('email', $email);
            					$this->db->update('tbl_emailsubcriber', $data);

            						$res_msg=	 $this->updateon_mailchip($email,$list_id,'daily');
                        $response['data']=array();
                        $response['message']= $res_msg;
                        $response['status']= 1;
                        $this->response($response, 200);


            				}else{
            					//add data in both db and mailchip
            					$data = array(
            				   'email' => $email ,
            				   'subscribe_type' => 'daily',
            					 'list_id'=>$list_id
            				);
            				$this->db->insert('tbl_emailsubcriber', $data);
            				if($this->db->affected_rows()){
            					 $res_msg=	 $this->addon_mailchip($email,$first_name,$last_name,$list_id,'daily');
                       $response['data']=array();
                       $response['message']= $res_msg;
                       $response['status']= 1;
                       $this->response($response, 200);

            				}else{
            					//throw error to frond end
                      $response['data']=array();
                      $response['message']= 'Opps!... getting error';
                      $response['status']= 0;
                      $this->response($response, 200);
            				}


            				}
            			break;
            			case 'weekly':
            					$query = $this->db->get_where('tbl_emailsubcriber',array('email'=>$email));
            					if($query->num_rows() > 0){
            						//update data in both db and mailchip
            						$data = array(
            						 'subscribe_type' => 'weekly'
            						);
            						$this->db->where('email', $email);
            						$this->db->update('tbl_emailsubcriber', $data);

            							$res_msg=	 $this->updateon_mailchip($email,$list_id,'weekly');
                          $response['data']=array();
                          $response['message']= $res_msg;
                          $response['status']= 1;
                          $this->response($response, 200);


            					}else{
            						//add data in both db and mailchip
            						$data = array(
            					   'email' => $email ,
            					   'subscribe_type' => 'weekly',
            						 'list_id'=>$list_id
            					);
            					$this->db->insert('tbl_emailsubcriber', $data);
            					if($this->db->affected_rows()){
            						 $res_msg=	 $this->addon_mailchip($email,$first_name,$last_name,$list_id,'weekly');
                         $response['data']=array();
                         $response['message']= $res_msg;
                         $response['status']= 1;
                         $this->response($response, 200);

            					}else{
            						//throw error to frond end
                        $response['data']=array();
                        $response['message']= 'Opps ! getting error';
                        $response['status']= 0;
                        $this->response($response, 200);
            					}


            					}
            				break;
            				case 'biweekly':
            						$query = $this->db->get_where('tbl_emailsubcriber',array('email'=>$email));
            						if($query->num_rows() > 0){
            							//update data in both db and mailchip
            							$data = array(
            							 'subscribe_type' => 'biweekly'
            							);
            							$this->db->where('email', $email);
            							$this->db->update('tbl_emailsubcriber', $data);

            								$res_msg=	 $this->updateon_mailchip($email,$list_id,'biweekly');
                            $response['data']=array();
                            $response['message']= $res_msg;
                            $response['status']= 1;
                            $this->response($response, 200);


            						}else{
            							//add data in both db and mailchip
            							$data = array(
            						   'email' => $email ,
            						   'subscribe_type' => 'biweekly',
            							 'list_id'=>$list_id
            						);
            						$this->db->insert('tbl_emailsubcriber', $data);
            						if($this->db->affected_rows()){
            							 $res_msg=	 $this->addon_mailchip($email,$first_name,$last_name,$list_id,'biweekly');
                           $response['data']=array();
                           $response['message']= $res_msg;
                           $response['status']= 1;
                           $this->response($response, 200);

            						}else{
            							//throw error to frond end
                          $response['data']=array();
                          $response['message']= 'Opps ! getting error ';
                          $response['status']= 0;
                          $this->response($response, 200);
            						}


            						}
            					break;
            					case 'monthly':
            							$query = $this->db->get_where('tbl_emailsubcriber',array('email'=>$email));
            							if($query->num_rows() > 0){
            								//update data in both db and mailchip
            								$data = array(
            								 'subscribe_type' => 'monthly'
            								);
            								$this->db->where('email', $email);
            								$this->db->update('tbl_emailsubcriber', $data);

            									$res_msg=	 $this->updateon_mailchip($email,$list_id,'monthly');
                              $response['data']=array();
                              $response['message']= $res_msg;
                              $response['status']= 1;
                              $this->response($response, 200);


            							}else{
            								//add data in both db and mailchip
            								$data = array(
            							   'email' => $email ,
            							   'subscribe_type' => 'monthly',
            								 'list_id'=>$list_id
            							);
            							$this->db->insert('tbl_emailsubcriber', $data);
            							if($this->db->affected_rows()){
            								 $res_msg=	 $this->addon_mailchip($email,$first_name,$last_name,$list_id,'monthly');
                             $response['data']=array();
                             $response['message']= $res_msg;
                             $response['status']= 1;
                             $this->response($response, 200);

            							}else{
            								//throw error to frond end
                            $response['data']=array();
                            $response['message']= 'Opps ! getting error';
                            $response['status']= 0;
                            $this->response($response, 200);
            							}


            							}
            						break;
            						case 'unsubscribed':
            						$this->db->delete('tbl_emailsubcriber', array('email' => $email));
            						$res_msg=	 $this->unsubscribe_mailchip($email,$list_id);
                        $response['data']=array();
                        $response['message']= $res_msg;
                        $response['status']= 1;
                        $this->response($response, 200);

            						break;




            	}
            	//-------------------------
              //------------------------------------------

            } //end of  mailchip api
            //this api  is used to return email subscrption check by tokenid
            function getEmailSubscriptionStatus_post(){
              $header_data=$this->input->request_headers();
              $token_withuserid=$header_data['iggi_token'];
              $token_arr = explode("18IGGI$", $token_withuserid);
              $user_id      = $token_arr[1];
              $email_subs_arr=getUserById($user_id);
              if(empty($token_withuserid)){
                $response['data']=array();
                $response['message']= 'Token missing';
                $response['status']= 0;
                $this->response($response, 200);
              }
              if(empty($user_id)){
                $response['data']=array();
                $response['message']= 'Invalid Token';
                $response['status']= 0;
                $this->response($response, 200);
              }
              $query = $this->db->get_where('tbl_emailsubcriber',array('email'=>$email_subs_arr[0]->email));
              $arr_data = array();
              $data_arr=$query->result();
              $arr_data['subscription_type']=$data_arr[0]->subscribe_type;
              if($query->num_rows() > 0){
                 $response['data']=$arr_data;
                 $response['message']= 'Email Subscription Type';
                 $response['status']= 1;
                 $this->response($response, 200);
              }else{
                $response['data']=array();
                $response['message']= 'No record found';
                $response['status']= 0;
                $this->response($response, 200);
              }


            }

            //this api is used to show menteelist by mentor tokenid
            function getMenteeListByMentor_post(){
              $header_data=$this->input->request_headers();
              $token_withuserid=$header_data['iggi_token'];
              $token_arr = explode("18IGGI$", $token_withuserid);
              $user_id      = $token_arr[1];
              $email_subs_arr=getUserById($user_id);
              if(empty($token_withuserid)){
                $response['data']=array();
                $response['message']= 'Token missing';
                $response['status']= 0;
                $this->response($response, 200);
              }
              if(empty($user_id)){
                $response['data']=array();
                $response['message']= 'Invalid Token';
                $response['status']= 0;
                $this->response($response, 200);
              }

            $mentorid=$user_id;
            $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"1",'added_by'=>$mentorid,'user_type'=>"1"));
            if($query->num_rows() > 0){
              $user_arr=$query->result();
              $user_data_arr = array();
              $arrayFbSocial = array();
              $data_arr_show = array();
              foreach ($user_arr as $key => $value) {
                $mentee_id=$value->uid;
                $usr_data=getUserById($mentee_id);
                $pro_pic_arr=json_decode($usr_data[0]->profile_pic);
                $pro_phto=$usr_data[0]->profile_pic;
                 if(empty($pro_phto)){
                    $pro_img_url=BASE."uploads/profile/avator-default.png";
                 }else{
                   if($pro_pic_arr->pic_from=='self'){
                     $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
                   }else{
                     $pro_img_url= $result_arr_new[0]->profile_pic;
                   }
                 }


                 //-------------------
                 $gooleplus_arr=getGooglePLUSUser($mentee_id);
                 $gooleplus_arr = array_filter($gooleplus_arr);
                 $gplus=0;
                 if (!empty($gooleplus_arr)) {
                   $gplus=1;
                 }
                 $instagram_arr=getInstagramUser($mentee_id);
                 $instagram_arr = array_filter($instagram_arr);
                 $inst=0;
                 if (!empty($instagram_arr)) {
                   $inst=1;
                 }


                 $linkedin_arr=getLinkedinUser($mentee_id);
                 $linkedin_arr = array_filter($linkedin_arr);
                 $li=0;
                 if (!empty($linkedin_arr)) {
                     $li=1;
                 }

                 $facebook_arr=getFacebookUser($mentee_id);
                 $facebook_arr = array_filter($facebook_arr);
                 $fb=0;
                 if (!empty($facebook_arr)) {
                   $fb=1;
                 }
                   $twitter_arr=getTwitterUser($mentee_id);
                   $twitter_arr = array_filter($twitter_arr);
                   $tw=0;
                   if (!empty($twitter_arr)) {
                     $tw=1;
                   }
                   $arrayFbSocial = array(
                     'facebook' =>$fb ,
                     'twitter' =>$tw ,
                     'linkedin'=>$li,
                     'instagram'=>$inst,
                     'google_plus'=>$gplus,

                   );
                 //-------------------





                $user_data_arr=array(
                'user_id'=>$usr_data[0]->id,
                'first_name'=>$usr_data[0]->first_name,
                'last_name'=>$usr_data[0]->last_name,
                'email'=>$usr_data[0]->email,
                'mentor_type'=>$usr_data[0]->mentor_type,
                'profile_pic'=>$pro_img_url,
                'mentee_post_permission'=>$usr_data[0]->mentee_post_permission,
                'mentee_archive_check'=>$usr_data[0]->mentee_archive

               );
               $data_arr_show[]=array_merge($user_data_arr,$arrayFbSocial);
              }
               $response['data']=$data_arr_show ;
               $response['message']= " List of mentee with mentee socila media added check";
               $response['status']= 1;
             }else{
               $response['data']= array();
               $response['message']="No mentee found";
               $response['status']= 0;
             }
            $this->response($response, 200);
           }

           //this function is used to invitation mentee and send code  for invitation
             function menteeInvitation_post(){
               $header_data=$this->input->request_headers();
               $token_withuserid=$header_data['iggi_token'];
               $token_arr = explode("18IGGI$", $token_withuserid);
               $user_id      = $token_arr[1];
               $email_subs_arr=getUserById($user_id);
               if(empty($token_withuserid)){
                 $response['data']=array();
                 $response['message']= 'Token missing';
                 $response['status']= 0;
                 $this->response($response, 200);
               }
               if(empty($user_id)){
                 $response['data']=array();
                 $response['message']= 'Invalid Token';
                 $response['status']= 0;
                 $this->response($response, 200);
               }
              $email= $this->post('email');
              $plan_feature_arr=getPlanFeatureByuserID($user_id);
             	$plan_featureUsed_arr=getPlanFeatureUsedByuserID($user_id);
              if($plan_featureUsed_arr['mentee_number']>=$plan_feature_arr['mentee_number']){

                 $response['data']= array();
                 $response['message']= 'Upgrate Your plan . excced mentee invitation';
                 $response['status']= 0;
                 $this->response($response, 200);
             }else{
               $invite_code   =$this->display_uni();
               $usr_arr=getUserById($user_id);
               $usertypeid='1';
             		$query = $this->db->get_where('tbl_mentor_mentee',array('email'=>$email,'status'=>'1'));
             		if($query->num_rows() > 0){
                   $response['data']= array();
                   $response['message']= 'Already Member of IGGI';
                   $response['status']= 0;
                   $this->response($response, 200);
             		}else{
             		$this->load->library('encrypt');
             		$activelinkkey=$this->encrypt->encode($key_active);
             		$this->load->library('SendEmail', null, 'mailObj');
             		$strpEmailTo = $email;
             		$strpRecipientName = '';
             		$arrpTags = array();
             		$arrpTags['{--RecipientName--}'] = $strpRecipientName;
             		$arrpTags['{--SiteHomeURL--}'] = base_url();
             		$arrpTags['{--SiteHomeURLlogo--}'] = BASE."theme/images/logo.png";
             		$arrpTags['{--EmailId--}'] = $email;
             		$arrpTags['{--Subject--}'] = "Invitaion Code";
             		$arrpTags['{--LoginUrl--}'] = base_url();
             		$arrpTags['{--invitationcode--}'] =$invite_code;
             		$signup_link=BASE."index.php/mentee/signup_link/".$usr_arr[0]->id;

             		$arrpTags['{--InvitedBy--}'] = '<a href="'.$signup_link.'" title="'.$usr_arr[0]->email.'">"SignUp Now"</a>';
             		$strpTempleteid = 1;
             		if($this->mailObj->sendEmail(
             				$strpEmailTo,
             				$strpRecipientName,
             				$arrpTags,
             				$strpTempleteid,
             				$AdminEmailId = '',
             				$AdminName = '')) ;
             				$query = $this->db->get_where('tbl_mentor_mentee',array('email'=>$email,'added_by'=>$user_id));
             				if($query->num_rows() > 0){
             				//update code
             				$data = array(
             					'created_on' => date('Y-m-d'),
             					'user_type'=>$usertypeid,
             					'activelinkkey'=>$invite_code
             				);
             				$this->db->update('tbl_mentor_mentee', $data, array('added_by' => $user_id,'status'=>'0'));
             				}else{
             					//insert code
             					$data = array(
             				 		'user_type'=>$usertypeid,
             				 		'added_by' => $user_id,
             				 		'status' => '0' ,
             				 		'created_on' => date('Y-m-d h:i:s'),
             				 		'activelinkkey'=>$invite_code,
             				 		'email'=>$email
             				  );
             				 $lid= $this->db->insert('tbl_mentor_mentee', $data);

             					$sql = $this->db->last_query();


             				}
                    //-------------get list of mentee pendingid
                    $mentorid=$user_id;
                    $user_data_arr=array();

                    $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"0",'added_by'=>$mentorid));
                     if($query->num_rows() > 0){
                     $usr_data=$query->result();
                     foreach ($usr_data as $key => $value) {

                        $user_data_arr[]=array(
                        'invite_id'=>intval($value->id),
                        'email'=>$value->email,
                        'invite_date'=>'May 2,2018'

                       );
                     }
                   }

                     $response['data']= $user_data_arr;
                     $response['message']= 'Invitation sent successfully';
                     $response['status']= 1;
                     $this->response($response, 200);
             			}

             }



          }//end of invite mentee
          //this api is for list of pending invition of mentee by mentor send
          function getPendingListofMenteeInvitation_post(){

            $header_data=$this->input->request_headers();
            $token_withuserid=$header_data['iggi_token'];
            $token_arr = explode("18IGGI$", $token_withuserid);
            $user_id      = $token_arr[1];
            $email_subs_arr=getUserById($user_id);
            if(empty($token_withuserid)){
              $response['data']=array();
              $response['message']= 'Token missing';
              $response['status']= 0;
              $this->response($response, 200);
            }
            if(empty($user_id)){
              $response['data']=array();
              $response['message']= 'Invalid Token';
              $response['status']= 0;
              $this->response($response, 200);
            }

             $mentorid=$user_id;

              $user_data_arr=array();

              $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"0",'added_by'=>$mentorid));
               if($query->num_rows() > 0){
               $usr_data=$query->result();
               foreach ($usr_data as $key => $value) {

                  $user_data_arr[]=array(
                  'invite_id'=>intval($value->id),
                  'email'=>$value->email,
                  'invite_date'=>'May 2,2018'

                 );
               }
                  $response['data']=$user_data_arr;
                  $response['message']= " List of pending Mentee";
                  $response['status']= 1;

              }else{
                $response['data']=array();
                $response['message']= " No pending mentee list";
                $response['status']= 0;
              }

                 $this->response($response, 200);

          }

          //this api is used to cancel invitation from mentee
          function cancelMenteeInvitation_post(){
              $invite_id      = $this->post('invite_id');
              $query = $this->db->get_where('tbl_mentor_mentee',array('id'=>$invite_id));
              $header_data=$this->input->request_headers();
              $token_withuserid=$header_data['iggi_token'];
              $token_arr = explode("18IGGI$", $token_withuserid);
              $user_id      = $token_arr[1];
              $email_subs_arr=getUserById($user_id);
              if(empty($token_withuserid)){
                $response['data']=array();
                $response['message']= 'Token missing';
                $response['status']= 0;
                $this->response($response, 200);
              }
              if(empty($user_id)){
                $response['data']=array();
                $response['message']= 'Invalid Token';
                $response['status']= 0;
                $this->response($response, 200);
              }
              $mentorid=$user_id;
              $user_data_arr=array();
              $query = $this->db->get_where('tbl_mentor_mentee',array('status'=>"0",'added_by'=>$mentorid));
                 if($query->num_rows() > 0){
                 $usr_data=$query->result();
                 foreach ($usr_data as $key => $value) {
                    $user_data_arr[]=array(
                    'invite_id'=>intval($value->id),
                    'email'=>$value->email,
                    'invite_date'=>$value->created_on

                   );
                 }
               }
               if($query->num_rows() > 0){
                 $this->db->delete('tbl_mentor_mentee', array('id' => $invite_id));
                 $response['data']=$user_data_arr;
                 $response['message']= "You have successfully cancel invitation";
                 $response['status']= 1;
               }else{
                 $response['data']=array();
                 $response['message']= "no invitation found to cancel";
                 $response['status']= 1;
               }
               $this->response($response, 200);

          }
          //this api is used to provide
          function postApprovedStatus_post(){
            $header_data=$this->input->request_headers();
            $token_withuserid=$header_data['iggi_token'];
            $token_arr = explode("18IGGI$", $token_withuserid);
            $user_id      = $token_arr[1];
            $email_subs_arr=getUserById($user_id);
           $approve_status      = $this->post('approve_status');

            if(empty($token_withuserid)){
              $response['data']=array();
              $response['message']= 'Token missing';
              $response['status']= 0;
              $this->response($response, 200);
            }
            if(empty($user_id)){
              $response['data']=array();
              $response['message']= 'Invalid Token';
              $response['status']= 0;
              $this->response($response, 200);
            }

            $data = array(
               'mentee_post_permission' =>$approve_status==0 ? '0' :'1'
            );
            $this->db->where('id', $user_id);
            $this->db->update('user_management', $data);
            $response['data']=array();
            $response['message']= "Permission set successfully";
            $response['status']= 1;
            $this->response($response, 200);

          }
//this api is used to show profile details by tokenid
function getUserProfileDetails_post(){
  $header_data=$this->input->request_headers();
  $token_withuserid=$header_data['iggi_token'];
  $token_arr = explode("18IGGI$", $token_withuserid);
  $user_id      = $token_arr[1];
  $email_subs_arr=getUserById($user_id);
 $approve_status      = $this->post('approve_status');

  if(empty($token_withuserid)){
    $response['data']=array();
    $response['message']= 'Token missing';
    $response['status']= 0;
    $this->response($response, 200);
  }
  if(empty($user_id)){
    $response['data']=array();
    $response['message']= 'Invalid Token';
    $response['status']= 0;
    $this->response($response, 200);
  }
  $query = $this->db->get_where('user_management',array('id'=>$user_id));
  $arr_data = array();
  if($query->num_rows() > 0){
     $result_arr_new=$query->result();

     $pro_pic_arr=json_decode($result_arr_new[0]->profile_pic);
     $pro_phto=$result_arr_new[0]->profile_pic;
     if(empty($pro_phto)){
        $pro_img_url=BASE."uploads/profile/avator-default.png";
     }else{
       if($pro_pic_arr->pic_from=='self'){
         $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
       }else{
         $pro_img_url= $result_arr_new[0]->profile_pic;
       }
     }
     if(empty($result_arr_new[0]->iphone_devive_id)){
       $ios_device_status=0;
     }else{
       $ios_device_status=1;
     }
     if(empty($result_arr_new[0]->android_devive_id)){
       $android_devive_id_status=0;
     }else{
       $android_devive_id_status=1;
     }
     $user_arr=array(
       'email'=>$result_arr_new[0]->email,
       'status'=>$result_arr_new[0]->status,
       'user_type'=>intval($result_arr_new[0]->user_type),
       'post_right'=>intval($result_arr_new[0]->post_right),
       'first_name'=>$result_arr_new[0]->first_name,
       'last_name'=>$result_arr_new[0]->last_name,
       'mentor_type'=>is_null($result_arr_new[0]->mentor_type) ? '':$result_arr_new[0]->mentor_type,
       'org_type'=>is_null($result_arr_new[0]->org_type) ? '':$result_arr_new[0]->org_type,
       'profile_pic'=>$pro_img_url,
       'mentor_type_other'=>is_null($result_arr_new[0]->mentor_type_other) ? '':$result_arr_new[0]->mentor_type_other,

       'ios_device_status'=>$ios_device_status,
       'android_devive_id_status'=>$android_devive_id_status,
       'profile_complete_check'=>$result_arr_new[0]->first_name="" ? 0:1

     );
     $response['data']=$user_arr;
     $response['message']= "Invalid Authorization";
     $response['status']= 1;
     $this->response($response, 200);

  }else{
    $response['data']=array();
    $response['message']= "Invalid Authorization";
    $response['status']= 1;
    $this->response($response, 200);
  }

}
//this api is used to archive mentee or remove mentee by mentee id
function setMenteeArchivedOrActive_post(){
  $menteeid      = $this->post('menteeid');
  $event_action      = $this->post('event_action');
  if(empty($menteeid)){
    $response['data']=array();
    $response['message']= "mentee id required";
    $response['status']= 0;
    $this->response($response, 200);
  }
  $data_arr = array(
       'mentee_archive' =>$event_action
  );
  $this->db->where('id', $menteeid);
  $this->db->update('user_management', $data_arr);

  $response['data']=array();
  $response['message']= "Mentee Information Updated";
  $response['status']= 1;
  $this->response($response, 200);

}
//this api is used to save chat loggeduser and to buddy user

function addInboxMessage_post(){
     $chat_from      = $this->post('chat_from');
     $chat_to      = $this->post('chat_to');
     $chat_message  = $this->post('chat_message');
     $chat_type      = $this->post('chat_type');

    if($chat_from=="" ){
      $response['data']=array();
      $response['message']= "Invalid Input";
      $response['status']= 0;
      $this->response($response, 200);
    }elseif($chat_to=="" ){
      $response['data']=array();
      $response['message']= "Invalid Input.";
      $response['status']= 0;
      $this->response($response, 200);
    }else{
      $data = array(
        'chat_from'	=> $chat_from,
        'chat_to'	=> $chat_to,
        'chat_message'=>$chat_message,
        'chat_type'		=> $chat_type

      );

      $this->db->insert('tbl_chat', $data);
      $response['data']=array();
      $response['message']= "Inbox chat added ";
      $response['status']= 0;
      $this->response($response, 200);
    }
}
function getInboxmessage_post(){
  $logged_user=post('loggeduser');
  $buddy_user=$this->input->post('buddyuser');
  $limit = 10;
  $this->db->where('chat_from', $logged_user);
  $this->db->where('chat_to', $buddy_user);
  $this->db->or_where('chat_from', $buddy_user);
  $this->db->where('chat_to', $logged_user);
  $this->db->order_by('id', 'desc');
  $messages = $this->db->get('tbl_chat', $limit);

  $this->db->where('chat_to', $logged_user)->where('chat_from',$buddy_user)->update('tbl_chat', array('is_read'=>'1'));
  $result= $messages->result();
  foreach ($result as $key_1 => $value_1) {
    $postime=date_time_ago($value_1->created_at);
    $user_arr=getUserById($value_1->chat_from);
    $pro_pic_arr=json_decode($usr_arr[0]->profile_pic);
    $pro_phto=$usr_arr[0]->profile_pic;
    if(empty($pro_phto)){
       $pro_img_url=BASE."uploads/profile/avator-default.png";
    }else{
      if($pro_pic_arr->pic_from=='self'){
        $pro_img_url=BASE."uploads/profile/".$pro_pic_arr->pic_name_url;
      }else{
        $pro_img_url= $result_arr_new[0]->profile_pic;
      }
    }

    $new_chat_arr[] = (object)array(
            "id"=>intval($value_1->id),
            "chat_from"=>intval($value_1->chat_from),
            "chat_to"=>intval($value_1->chat_to),
            "message"=>$value_1->chat_message,
            "is_read"=>intval($value_1->is_read),
            "time"=>$postime,
            "message_type"=>$value_1->chat_type,
            'from_name'=>$user_arr[0]->first_name
          );

  }
  $response['data']=$new_chat_arr;
  $response['message']= "Get Chat history";
  $response['status']= 1;
  $this->response($response, 200);

}











}
//end of Api2 class
